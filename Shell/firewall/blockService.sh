#!/bin/bash

if [ $# -eq 2 ]
then
  echo "Vous venez de bloquer le protocol $1 et le port $2"
  iptables -I INPUT -p $1 --dport $2 -j DROP
else
  echo "Aucune ip ou port n'a �t� renseign�!"
  echo "La commande doit �tre similaire � '.\blockService.sh tcp 22'"
fi
