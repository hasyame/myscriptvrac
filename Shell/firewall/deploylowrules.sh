#!/bin/bash
#BENOIT BREUL
#24/07/2019
# V1.0
##############

packet="iptables-persistent"
value=`dpkg-query -W -f='${Status}\n' $packet`

echo "Ex�cution du script de firewall...!"

if [[ "$value" = "install ok installed" ]]
then
        echo "Iptables-persistent est bien intall�! Nous allons maintenant configurer par d�faut le parefeu linux...!"
        echo "Modification du port SSH par d�faut! (Num�ro du port: 3601)"
        sudo sed -i 's/#Port 22/Port 3601/g' /etc/ssh/sshd_config
        echo "Rechargement du service SSH"
        sudo service ssh reload
        echo "Ajout des r�gles iptables!"
        sudo iptables -P INPUT ACCEPT
        sudo iptables -P FORWARD ACCEPT
        sudo iptables -P OUTPUT ACCEPT
        sudo iptables -F
        sudo iptables -X
        sudo iptables -t nat -F
        sudo iptables -t nat -X
        sudo iptables -t mangle -F
        sudo iptables -t mangle -X
        sudo iptables -N syn-flood
        sudo iptables -N port-scan
        sudo iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
        sudo iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
        sudo iptables -t filter -A INPUT -i lo -j ACCEPT
        sudo iptables -t filter -A OUTPUT -o lo -j ACCEPT
        sudo iptables -t filter -A OUTPUT -p tcp --sport 53 -j ACCEPT
        sudo iptables -t filter -A OUTPUT -p udp --sport 53 -j ACCEPT
        sudo iptables -t filter -A INPUT -p tcp --dport 53 -j ACCEPT
        sudo iptables -t filter -A INPUT -p udp --dport 53 -j ACCEPT
        sudo iptables -t filter -A OUTPUT -p udp --sport 123 -j ACCEPT
        sudo iptables -A INPUT -p icmp -m icmp --icmp-type 0 -m conntrack --ctstate NEW -j ACCEPT
        sudo iptables -A INPUT -p icmp -m icmp --icmp-type 3 -m conntrack --ctstate NEW -j ACCEPT
        sudo iptables -A INPUT -p icmp -m icmp --icmp-type 8 -m conntrack --ctstate NEW -j ACCEPT
        sudo iptables -A INPUT -p icmp -m icmp --icmp-type 11 -m conntrack --ctstate NEW -j ACCEPT
        sudo iptables -A INPUT -p tcp -m tcp --dport 3601 -j ACCEPT
        sudo iptables -A OUTPUT -p tcp -m tcp --sport 3601 -j ACCEPT
        sudo iptables -A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG NONE -j LOG --log-prefix "IPTABLES NULL-SCAN:"
        sudo iptables -A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,SYN,RST,PSH,ACK,URG -j LOG --log-prefix "IPTABLES XMAS-SCAN:"
        sudo iptables -A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,SYN -j LOG --log-prefix "IPTABLES SYNFIN-SCAN:"
        sudo iptables -A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,PSH,URG -j LOG --log-prefix "IPTABLES NMAP-XMAS-SCAN:"
        sudo iptables -A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN -j LOG --log-prefix "IPTABLES FIN-SCAN:"
        sudo iptables -A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,SYN,PSH,URG -j LOG --log-prefix "IPTABLES NMAP-ID:"
        sudo iptables -A INPUT -p tcp -m tcp --tcp-flags SYN,RST SYN,RST -j LOG --log-prefix "IPTABLES SYN-RST:"
        sudo iptables -A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -j syn-flood
        sudo iptables -A INPUT -p tcp -m tcp ! --tcp-flags FIN,SYN,RST,ACK SYN -m state --state NEW -j LOG --log-prefix "IPTABLES SYN-FLOOD:"
        sudo iptables -A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK RST -j port-scan
        sudo iptables -A OUTPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG NONE -j LOG --log-prefix "IPTABLES NULL-SCAN:"
        sudo iptables -A OUTPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,SYN,RST,PSH,ACK,URG -j LOG --log-prefix "IPTABLES XMAS-SCAN:"
        sudo iptables -A OUTPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,SYN -j LOG --log-prefix "IPTABLES SYNFIN-SCAN:"
        sudo iptables -A OUTPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,PSH,URG -j LOG --log-prefix "IPTABLES NMAP-XMAS-SCAN:"
        sudo iptables -A OUTPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN -j LOG --log-prefix "IPTABLES FIN-SCAN:"
        sudo iptables -A OUTPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,SYN,PSH,URG -j LOG --log-prefix "IPTABLES NMAP-ID:"
        sudo iptables -A OUTPUT -p tcp -m tcp --tcp-flags SYN,RST SYN,RST -j LOG --log-prefix "IPTABLES SYN-RST:"
        sudo iptables -A OUTPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -j syn-flood
        sudo iptables -A OUTPUT -p tcp -m tcp ! --tcp-flags FIN,SYN,RST,ACK SYN -m state --state NEW -j LOG --log-prefix "IPTABLES SYN-FLOOD:"
        sudo iptables -A OUTPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK RST -j port-scan
        sudo iptables -A OUTPUT -m conntrack ! --ctstate INVALID -j ACCEPT
        sudo iptables -A INPUT -p tcp -m tcp --dport 80 -j ACCEPT
        sudo iptables -A OUTPUT -p tcp -m tcp --sport 80 -j ACCEPT
        sudo iptables -A INPUT -p tcp -m tcp --dport 443 -j ACCEPT
        sudo iptables -A OUTPUT -p tcp -m tcp --sport 443 -j ACCEPT
        sudo iptables -t raw -A PREROUTING -p tcp -m multiport --dports 22,80,443 -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -j CT --notrack
        sudo iptables -t raw -A PREROUTING -i eth0 -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -m multiport --dports 22,80,443 -m hashlimit --hashlimit-above 200/sec --hashlimit-burst 1000 --hashlimit-mode srcip --hashlimit-name syn --hashlimit-htable-size 2097152 --hashlimit-srcmask 24 -j DROP
        sudo iptables -t mangle -A PREROUTING -p tcp -m tcp --tcp-flags FIN,SYN FIN,SYN -j DROP
        sudo iptables -t mangle -A PREROUTING -p tcp -m tcp --tcp-flags SYN,RST SYN,RST -j DROP
        sudo iptables -t mangle -A PREROUTING -p tcp -m tcp --tcp-flags FIN,RST FIN,RST -j DROP
        sudo iptables -t mangle -A PREROUTING -p tcp -m tcp --tcp-flags FIN,ACK FIN -j DROP
        sudo iptables -t mangle -A PREROUTING -p tcp -m tcp --tcp-flags ACK,URG URG -j DROP
        sudo iptables -t mangle -A PREROUTING -p tcp -m tcp --tcp-flags PSH,ACK PSH -j DROP
        sudo iptables -t mangle -A PREROUTING -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,SYN,RST,PSH,ACK,URG -j DROP
        sudo iptables -t mangle -A PREROUTING -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG NONE -j DROP
        sudo iptables -t mangle -A PREROUTING -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,PSH,URG -j DROP
        sudo iptables -t mangle -A PREROUTING -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,SYN,PSH,URG -j DROP
        sudo iptables -t mangle -A PREROUTING -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,SYN,RST,ACK,URG -j DROP
        sudo iptables -t mangle -A PREROUTING -s 224.0.0.0/8 -j DROP
        sudo iptables -A PREROUTING -s 169.254.0.0/16 -j DROP
        sudo iptables -A PREROUTING -s 172.16.0.0/12 -j DROP
        sudo iptables -t mangle -A PREROUTING -s 192.0.2.0/24 -j DROP
        sudo iptables -A PREROUTING -s 192.168.0.0/16 -j DROP
        sudo iptables -A PREROUTING -s 10.0.0.0/8 -j DROP
        sudo iptables -A PREROUTING -s 0.0.0.0/8 -j DROP
        sudo iptables -t mangle -A PREROUTING -s 240.0.0.0/5 -j DROP
        sudo iptables -t mangle -A PREROUTING -s 127.0.0.0/8 ! -i lo -j DROP
        echo "R�gles par d�faut mises en place! Modification des policies sur DROP"
        sudo iptables -t filter -P INPUT DROP
        sudo iptables -t filter -P FORWARD DROP
        sudo iptables -t filter -P OUTPUT DROP
        echo "Ajout de la persistence des r�gles"
        sudo iptables-save -c > /etc/iptables/rules.v4
        echo "Rechargement du service netfilter"
        sudo service netfilter-persistent restart
        echo "FIN!"

else
        echo "Il vous manque le paquet $packet, arr�t!"
        exit 1
fi
