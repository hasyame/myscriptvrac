#!/bin/bash
# Benoit Breul

# Variables
http=80
https=443
ftp=21
dns=53

# Fonctions
good_pratices () {
  if [ $port -eq $1 ]
  then
    i=1
    while [ $i -eq 1 ]
      do
        if [ $port -eq $1 ]
        then
          echo "Vous utilisez le port par d�faut! Il n'est pas recommand� de laisser le port par d�faut!"
          echo "�tes-vous s�r d'utiliser le port $1 ? [Y/N]"
          read records
          if [ $records == "N" ]
          then
            echo "Num�ro du port $2 ? [ENTER]"
            read port
              if [ $port -ne $1 ]
                then
                  echo "Le port � bien �t� enregistr�!"
                  i=0
              fi
          elif [[ $records == "Y" ]]; then
            echo "Le port $sshname restera celui par d�faut... Soit $port!"
            i=0
          fi
  else
    echo "Le port $sshname restera celui par d�faut... Soit $port!"
        fi
    done
  fi
  echo ""
}

port_change () {
  echo "Le port $portname est $portvalue. Est-ce correct? [Y/N]"
  read records
  if [[ $records == "N" ]]; then
    echo "Nouvelle valeur du $portname ? [ENTER]"
    read port
    echo "Le porte $portname sera $port !"
  elif [[ $records == "Y" ]]; then
    echo "Le porte $portname sera $portvalue !"
  else
    echo "Le porte $portname sera $portvalue !"
  fi
  echo ""
}

ip_spc () {
  echo "Faut-il limiter � une ip sp�cifique le port $port? [Y/N]"
  read records
  if [[ $records == "Y" ]]; then
    echo "Adresse ip pour le port $port? [ENTER]"
    read ipsec
    echo "Le port $port sera limit� � l'adresse $ipsec !"
  elif [[ $records == "N" ]]; then
    echo "Pas de limitation � une ip sp�cifique!"
  else
    echo "Adresse ip pour le port $port? [ENTER]"
    read ipsec
    echo "Le port $port sera limit� � l'adresse $ipsec !"
  fi
  echo ""
}

# Code
echo "Nous allons configurer basiquement le parefeu IPTABLES de votre serveur."
echo "Vous devez avoir les paquets suivants install�s: Sudo, i"
echo "Pour ce faire veuillez r�pondre � quelques questions:"
echo "Num�ro du port SSH ? [ENTER]"
read sshread
port=$sshread
portvalue='22'
portname='SSH'
good_pratices "$portvalue" "$portname"
ssh=$port
ip_spc "$port"
sshipIN="-s $ipsec"
sshipOUT="-d $ipsec"

echo "Num�ro du port HTTP est �gale � $http"
port=$http
portname='HTTP'
portvalue=$http
port_change "$portvalue" "$portname"
http=$port

echo "Num�ro du port HTTPS est �gale � $https"
port=$https
portname='HTTPS'
portvalue=$https
port_change "$portvalue" "$portname"
https=$port

echo "Num�ro du port DNS est �gale � $nds"
port=$dns
portname='HTTPS'
portvalue=$dns
port_change "$portvalue" "$portname"
dns=$port

echo "Pr�sence d'un HIDS ? [Y/N]"
read records
if [[ $records == "Y" ]]; then
  hidsportsec='1514'
  hidsport='514'
  echo "Ouverture des ports agent hids..."
  echo "Adresse IP du serveur de hids ?"
  read iphids
  hidsseclineIN="sudo iptables -t filter -A INPUT -s $iphids -i eth0 -p udp -m udp --dport $hidsportsec -j ACCEPT"
  hidsseclineOUT="sudo iptables -t filter -A OUTPUT -d $iphids -p udp --sport $hidsportsec -j ACCEPT"
  hidsTCPlineIN="sudo iptables -t filter -A INPUT -s $iphids -i eth0 -p tcp -m udp --dport $hidsport -j ACCEPT"
  hidsTCPlineOUT="sudo iptables -t filter -A OUTPUT -d $iphids -p tcp --sport $hidsport -j ACCEPT"
  hidsUDPlineIN="sudo iptables -t filter -A INPUT -s $iphids -i eth0 -p udp -m udp --dport $hidsport -j ACCEPT"
  hidsUDPlineOUT="sudo iptables -t filter -A OUTPUT -d $iphids -p udp --sport $hidsport -j ACCEPT"
elif [[ $records == "N" ]]; then
  echo "Pas d'HIDS, on passe � l'�tape suivante."
  hidsportsec='1514'
  hidsport='514'
  hidsseclineIN="sudo iptables -t filter -A INPUT -i eth0 -p udp -m udp --dport $hidsportsec -j DROP"
  hidsseclineOUT="sudo iptables -t filter -A OUTPUT -p udp --sport $hidsportsec -j DROP"
  hidsTCPlineIN="sudo iptables -t filter -A INPUT -i eth0 -p tcp -m udp --dport $hidsport -j DROP"
  hidsTCPlineOUT="sudo iptables -t filter -A OUTPUT -p udp --sport $hidsport -j DROP"
  hidsUDPlineIN="sudo iptables -t filter -A INPUT -i eth0 -p udp -m udp --dport $hidsport -j DROP"
  hidsUDPlineOUT="sudo iptables -t filter -A OUTPUT -p udp --sport $hidsport -j DROP"
else
  hidsportsec='1514'
  hidsport='514'
  echo "Ouverture des ports agent hids..."
  echo "Adresse IP du serveur de hids ?"
  read iphids
  hidsseclineIN="sudo iptables -t filter -A INPUT -s $iphids -i eth0 -p udp -m udp --dport $hidsportsec -j ACCEPT"
  hidsseclineOUT="sudo iptables -t filter -A OUTPUT -d $iphids -p udp --sport $hidsportsec -j ACCEPT"
  hidsTCPlineIN="sudo iptables -t filter -A INPUT -s $iphids -i eth0 -p tcp -m tcp --dport $hidsport -j ACCEPT"
  hidsTCPlineOUT="sudo iptables -t filter -A OUTPUT -d $iphids -p tcp --sport $hidsport -j ACCEPT"
  hidsUDPlineIN="sudo iptables -t filter -A INPUT -s $iphids -i eth0 -p udp -m udp --dport $hidsport -j ACCEPT"
  hidsUDPlineOUT="sudo iptables -t filter -A OUTPUT -d $iphids -p udp --sport $hidsport -j ACCEPT"
fi
echo ""

echo "Pr�sence d'un monitoring SNMP ? [Y/N]"
read records
if [[ $records == "Y" ]]; then
  snmp='161'
  snmptrap='162'
  echo "Ouverture des ports snmp..."
  echo "Adresse IP du serveur de monitoring ?"
  read ipmonitoring
  snmplineIN="sudo iptables -t filter -A INPUT -s $ipmonitoring -i eth0 -p udp -m udp --dport $snmp -j ACCEPT"
  snmplineOUT="sudo iptables -t filter -A OUTPUT -d $ipmonitoring -p udp --sport $snmp -j ACCEPT"
  snmptraplineINT="sudo iptables -t filter -A INPUT -s $ipmonitoring -i eth0 -p udp -m udp --dport $snmptrap -j ACCEPT"
  snmptraplineOUT="sudo iptables -t filter -A OUTPUT -d $ipmonitoring -p udp --spo(rt $snmptrap -j ACCEPT"
elif [[ $records == "N" ]]; then
  echo "Pas de monitoring, on passe � l'�tape suivante."
  snmp='161'
  snmptrap='162'
  snmplineIN="sudo iptables -t filter -A INPUT -i eth0 -p udp -m udp --dport $snmp -j DROP"
  snmplineOUT="sudo iptables -t filter -A OUTPUT -p udp --sport $snmp -j DROP"
  snmptraplineINT="sudo iptables -t filter -A INPUT -i eth0 -p udp -m udp --dport $snmptrap -j DROP"
  snmptraplineOUT="sudo iptables -t filter -A OUTPUT -p udp --sport $snmptrap -j DROP"
else
  snmp='161'
  snmptrap='162'
  echo "Ouverture des ports snmp..."
  echo "Adresse IP du serveur de monitoring ?"
  read ipmonitoring
  snmplineIN="sudo iptables -t filter -A INPUT -s $ipmonitoring -i eth0 -p udp -m udp --dport $snmp -j ACCEPT"
  snmplineOUT="sudo iptables -t filter -A OUTPUT -d $ipmonitoring -p udp --sport $snmp -j ACCEPT"
  snmptraplineINT="sudo iptables -t filter -A INPUT -s $ipmonitoring -i eth0 -p udp -m udp --dport $snmptrap -j ACCEPT"
  snmptraplineOUT="sudo iptables -t filter -A OUTPUT -d $ipmonitoring -p udp --sport $snmptrap -j ACCEPT"
fi
echo ""

echo "Nous installons les r�gles iptabes."
sudo iptables -P INPUT ACCEPT
sudo iptables -P FORWARD ACCEPT
sudo iptables -P OUTPUT ACCEPT
sudo iptables -F
sudo iptables -X
sudo iptables -t nat -F
sudo iptables -t nat -X
sudo iptables -t mangle -F
sudo iptables -t mangle -X
sudo iptables -N syn-flood
sudo iptables -N port-scan
sudo iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
sudo iptables -t filter -A INPUT -i lo -j ACCEPT
sudo iptables -t filter -A OUTPUT -o lo -j ACCEPT
sudo iptables -t filter -A OUTPUT -p tcp --sport 53 -j ACCEPT
sudo iptables -t filter -A OUTPUT -p udp --sport 53 -j ACCEPT
sudo iptables -t filter -A INPUT -p tcp --dport 53 -j ACCEPT
sudo iptables -t filter -A INPUT -p udp --dport 53 -j ACCEPT
sudo iptables -t filter -A OUTPUT -p udp --sport 123 -j ACCEPT
sudo iptables -A INPUT -p icmp -m icmp --icmp-type 0 -m conntrack --ctstate NEW -j ACCEPT
sudo iptables -A INPUT -p icmp -m icmp --icmp-type 3 -m conntrack --ctstate NEW -j ACCEPT
sudo iptables -A INPUT -p icmp -m icmp --icmp-type 8 -m conntrack --ctstate NEW -j ACCEPT
sudo iptables -A INPUT -p icmp -m icmp --icmp-type 11 -m conntrack --ctstate NEW -j ACCEPT
sudo iptables -A INPUT -p tcp -m tcp $sshipIN --dport $ssh -j ACCEPT
sudo iptables -A OUTPUT -p tcp -m tcp $sshipOUT --sport $ssh -j ACCEPT
$snmplineIN
$snmplineOUT
$snmptraplineINT
$snmptraplineOUT
$hidsseclineIN
$hidsseclineOUT
$hidsTCPlineIN
$hidsTCPlineOUT
$hidsUDPlineIN
$hidsUDPlineOUT
sudo iptables -A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG NONE -j LOG --log-prefix "IPTABLES NULL-SCAN:"
sudo iptables -A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,SYN,RST,PSH,ACK,URG -j LOG --log-prefix "IPTABLES XMAS-SCAN:"
sudo iptables -A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,SYN -j LOG --log-prefix "IPTABLES SYNFIN-SCAN:"
sudo iptables -A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,PSH,URG -j LOG --log-prefix "IPTABLES NMAP-XMAS-SCAN:"
sudo iptables -A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN -j LOG --log-prefix "IPTABLES FIN-SCAN:"
sudo iptables -A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,SYN,PSH,URG -j LOG --log-prefix "IPTABLES NMAP-ID:"
sudo iptables -A INPUT -p tcp -m tcp --tcp-flags SYN,RST SYN,RST -j LOG --log-prefix "IPTABLES SYN-RST:"
sudo iptables -A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -j syn-flood
sudo iptables -A INPUT -p tcp -m tcp ! --tcp-flags FIN,SYN,RST,ACK SYN -m state --state NEW -j LOG --log-prefix "IPTABLES SYN-FLOOD:"
sudo iptables -A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK RST -j port-scan
sudo iptables -A OUTPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG NONE -j LOG --log-prefix "IPTABLES NULL-SCAN:"
sudo iptables -A OUTPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,SYN,RST,PSH,ACK,URG -j LOG --log-prefix "IPTABLES XMAS-SCAN:"
sudo iptables -A OUTPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,SYN -j LOG --log-prefix "IPTABLES SYNFIN-SCAN:"
sudo iptables -A OUTPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,PSH,URG -j LOG --log-prefix "IPTABLES NMAP-XMAS-SCAN:"
sudo iptables -A OUTPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN -j LOG --log-prefix "IPTABLES FIN-SCAN:"
sudo iptables -A OUTPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,SYN,PSH,URG -j LOG --log-prefix "IPTABLES NMAP-ID:"
sudo iptables -A OUTPUT -p tcp -m tcp --tcp-flags SYN,RST SYN,RST -j LOG --log-prefix "IPTABLES SYN-RST:"
sudo iptables -A OUTPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -j syn-flood
sudo iptables -A OUTPUT -p tcp -m tcp ! --tcp-flags FIN,SYN,RST,ACK SYN -m state --state NEW -j LOG --log-prefix "IPTABLES SYN-FLOOD:"
sudo iptables -A OUTPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK RST -j port-scan
sudo iptables -A OUTPUT -m conntrack ! --ctstate INVALID -j ACCEPT
sudo iptables -A INPUT -p tcp -m tcp --dport $http -j ACCEPT
sudo iptables -A OUTPUT -p tcp --sport $http -j ACCEPT
sudo iptables -A INPUT -p tcp -m tcp --dport $https -j ACCEPT
sudo iptables -A OUTPUT -p tcp --sport $https -j ACCEPT
sudo iptables -t raw -A PREROUTING -p tcp -m multiport --dports $ssh, $http ,$https -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -j CT --notrack
sudo iptables -t raw -A PREROUTING -i eth0 -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -m multiport --dports $ssh, $http ,$https -m hashlimit --hashlimit-above 200/sec --hashlimit-burst 1000 --hashlimit-mode srcip --hashlimit-name syn --hashlimit-htable-size 2097152 --hashlimit-srcmask 24 -j DROP
sudo iptables -t mangle -A PREROUTING -p tcp -m tcp --tcp-flags FIN,SYN FIN,SYN -j DROP
sudo iptables -t mangle -A PREROUTING -p tcp -m tcp --tcp-flags SYN,RST SYN,RST -j DROP
sudo iptables -t mangle -A PREROUTING -p tcp -m tcp --tcp-flags FIN,RST FIN,RST -j DROP
sudo iptables -t mangle -A PREROUTING -p tcp -m tcp --tcp-flags FIN,ACK FIN -j DROP
sudo iptables -t mangle -A PREROUTING -p tcp -m tcp --tcp-flags ACK,URG URG -j DROP
sudo iptables -t mangle -A PREROUTING -p tcp -m tcp --tcp-flags PSH,ACK PSH -j DROP
sudo iptables -t mangle -A PREROUTING -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,SYN,RST,PSH,ACK,URG -j DROP
sudo iptables -t mangle -A PREROUTING -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG NONE -j DROP
sudo iptables -t mangle -A PREROUTING -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,PSH,URG -j DROP
sudo iptables -t mangle -A PREROUTING -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,SYN,PSH,URG -j DROP
sudo iptables -t mangle -A PREROUTING -p tcp -m tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG FIN,SYN,RST,ACK,URG -j DROP
sudo iptables -t mangle -A PREROUTING -s 224.0.0.0/8 -j DROP
sudo iptables -t mangle -A PREROUTING -s 169.254.0.0/16 -j DROP
sudo iptables -t mangle -A PREROUTING -s 172.16.0.0/12 -j DROP
sudo iptables -t mangle -A PREROUTING -s 192.0.2.0/24 -j DROP
sudo iptables -t mangle -A PREROUTING -s 192.168.0.0/16 -j DROP
sudo iptables -t mangle -A PREROUTING -s 10.0.0.0/8 -j DROP
sudo iptables -t mangle -A PREROUTING -s 0.0.0.0/8 -j DROP
sudo iptables -t mangle -A PREROUTING -s 240.0.0.0/5 -j DROP
sudo iptables -t mangle -A PREROUTING -s 127.0.0.0/8 ! -i lo -j DROP
echo "R�gles par d�faut mises en place! Modification des policies sur DROP"
sudo iptables -t filter -P INPUT DROP
sudo iptables -t filter -P FORWARD DROP
sudo iptables -t filter -P OUTPUT DROP
echo "Ajout de la persistence des r�gles"
sudo iptables-save -c > /etc/iptables/rules.v4
echo "Rechargement du service netfilter"
sudo service netfilter-persistent restart

iptables -nvL
