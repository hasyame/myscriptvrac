#!/bin/bash

if [ $# -eq 3 ]
then
  echo "Vous venez de bloquer l'ip $1, le protocol $2 et le port $3"
  iptables -I INPUT -s $1 -p $2 --dport $3 -j DROP
else
  echo "Aucune ip ou port n'a �t� renseign�!"
  echo "La commande doit �tre similaire � '.\blockIPService.sh 10.0.0.1 tcp 22'"
fi
