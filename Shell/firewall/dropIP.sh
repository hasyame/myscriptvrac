#!/bin/bash

if [ $# -eq 1 ]
then
  echo "Vous venez de bloquer l'ip $1"
  iptables -I INPUT -s $1 -j DROP
else
  echo "Aucune ip n'a �t� renseign�!"
  echo "La commande doit �tre similaire � '.\dropIP.sh 10.0.0.1'"
fi
