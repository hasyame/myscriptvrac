#!/usr/bin/env python3
# Fonction "type" utilisé pour répondre au 2.1.2 ASVS
# Verify that passwords 64 characters or longer are permitted but may
# be no longer than 128 characters.
# Ce script est une amélioration du premier en empêchant un user de gen un mdp
# supérieur à 128 characters; Ceci réponds notamment partiellement aux risques
# de DoS
# ----------------------------------------------------------------------------
# Hasyame, 21/12/2020
# ----------------------------------------------------------------------------
import random
import string


def get_random_password(length):
    random_source = string.ascii_letters + string.digits + string.punctuation
    password = random.choice(string.ascii_lowercase)  # ajout 1 string MIN
    password += random.choice(string.ascii_uppercase)  # ajout 1 string MAJ
    password += random.choice(string.digits)  # ajout 1 string 1 -> 9
    password += random.choice(string.punctuation)  # ajout 1 string speciale

    for i in range(length):  # ajout des string, int et spe manquants aleatoire
        password += random.choice(random_source)

    password_list = list(password)  # creer une liste pour pouvoir mélanger
    random.SystemRandom().shuffle(password_list)  # mélange les valeurs
    password = ''.join(password_list)  # joint les valeurs de la liste
    return password  # print !


while True:
    try:
        mdplen = int(input("Longeur du mot de passe: "))
        if mdplen >= 12 and mdplen <= 128:
            mdplen = mdplen - 4
            break
        elif mdplen < 12:
            print("Longueur insuffisante.")
        elif mdplen > 128:
            print("Mot de passe trop long.")
        else:
            print("Une erreur inconnue est arrivé.")
    except ValueError:
        print("Utilisez un nombre pour répondre.")

print("Le mot de passe est ", get_random_password(mdplen))
