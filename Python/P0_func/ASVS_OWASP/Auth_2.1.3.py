#!/usr/bin/env python3
# Fonction "type" utilisé pour répondre au 2.1.2 ASVS
# Verify that password truncation is not performed. However,
# consecutive multiple spaces may be replaced by a single space.
# Ceci évite, lors de la génération random du mdp, de générer par mégarde un
# mdp avec une grande succession de la même string ou d'espace.
# ----------------------------------------------------------------------------
# Hasyame, 21/12/2020
# ----------------------------------------------------------------------------
import random
import string

# Fonction de génération du mot de passe


def get_random_password(length):
    # Variables pour traitement du password
    o = 1
    x = 0
    y = 1

    # Génération du mot de passe
    while True:
        rdm_src = string.ascii_letters + string.digits + string.punctuation
        password = random.choice(string.ascii_lowercase)  # ajout 1 string MIN
        password += random.choice(string.ascii_uppercase)  # ajout 1 string MAJ
        password += random.choice(string.digits)  # ajout 1 string 1 -> 9
        password += random.choice(string.punctuation)  # ajout 1 string special

        for i in range(length):  # ajout des string, int et spe aleatoire
            password += random.choice(rdm_src)

        pwd_list = list(password)  # creer une liste pour pouvoir mélanger
        random.SystemRandom().shuffle(pwd_list)  # mélange les valeurs

        # Comparaison des valeurs de la liste pour éviter les doublons
        listlen = len(pwd_list) - 1
        while o == 1:
            if x == listlen:
                o = 0
                password = ''.join(pwd_list)  # joint les valeurs de la liste
                return password
            elif pwd_list[x] == pwd_list[y]:
                break
            else:
                x = x + 1
                y = y + 1


if __name__ == '__main__':
    while True:
        try:
            mdplen = int(input("Longeur du mot de passe: "))
            if mdplen >= 12 and mdplen <= 128:
                mdplen = mdplen - 4
                break
            elif mdplen < 12:
                print("Longueur insuffisante.")
            elif mdplen > 128:
                print("Mot de passe trop long.")
            else:
                print("Une erreur inconnue est arrivé.")
        except ValueError:
            print("Utilisez un nombre pour répondre.")

    print("Le mot de passe est ", get_random_password(mdplen))
