#!/usr/bin/env python3
# Script permettant d'analyser et d'appuyer un premier niveau de
# confidentialité, d'intégrité et de disponiblité
import re
import os
import sys
import statistics
import csv
from bs4 import BeautifulSoup
import requests

print("~ Script d'analyse de FSV ~")
print("~        ++ ADSN ++       ~\n")
print("OK: Analyse du score CID")

# Variables générales
listeConfi = []
listeIntegr = []
listeDispo = []
listeVrais = []
val = ""
valint = 11
confidentialité = ""
intergrité = ""
disponiblité = ""
arrondisIntegr = ""
arrondisDispo = ""
arrondisConfi = ""

# Variables de niveau de disponibilité
nd1 = 1
nd2 = 2
nd3 = 3
nd4 = 4

# Variables de niveau d'intégrité
ni1 = 1
ni2 = 2
ni3 = 3
ni4 = 4

# Variables de niveau de confidentialité
nc1 = 1
nc2 = 2
nc3 = 3
nc4 = 4

# Variables de vraissemblance
nv1 = 1
nv2 = 2
nv3 = 3
nv4 = 4

# Récupère les informations de bases
eventName = input("Nom de l'évènement redouté ? : ")
eventName = eventName.strip()
fsvName = input("Nom de la FSV ? : ")
fsvName = fsvName.strip()
cveNumber = input("Nom de la/les CVE ?: ")
cveNumber = cveNumber.strip()
CVS = input("Score de base de la FSV: ")
CVS = CVS.strip()

# Récupération du chemin du Script
pathname = os.path.dirname(sys.argv[0])

# Variable des fichiers
# Chargement du dictionnaire
fdico_exist = pathname + "\\dico.txt"
if os.path.exists(fdico_exist):
    fdico = pathname + "\\dico.txt"
    print("OK: Dictionnaire chargé")
else:
    print("ERROR: Assurez-vous que dico.txt existe!")
    sys.exit()

# Variable des fichiers de résultats
fresultat = pathname + "\\fsv_analyse.csv"
ftemp = pathname + "\\~$fsv_analyse.csv"

fdico_read = open(fdico, encoding="latin-1").read().splitlines()

# Vérification si le fichier fsv analyse est ouvert...
while True:
    if os.path.exists(ftemp) is True:
        print("ERROR: Le fichier dsv_analyse.csv est ouvert!")
        x = input("Faite ENTRER quand vous avez fermé le fichier.")
    else:
        break

# Traitement du fichier à analyser
fanalyse_exist = pathname + "\\analyse.txt"
if os.path.exists(fanalyse_exist):
    print("Un fichier analyse.txt est présent à la racine")
    ifrem = str(input("Le fichier analyse.txt est-il le bon? [O/N] : "))
    if ifrem == "O":
        pass
    elif ifrem == "N":
        print("Suppression du fichier analyse.txt")
        os.remove(fanalyse_exist)
    else:
        print("Aucune réponses ne convient, le script continue.")

if os.path.exists(fanalyse_exist):
    fanalyse = pathname + "\\analyse.txt"
    print("OK: Chargement du fichier analayse.txt")
    with open(fanalyse, encoding="latin-1") as fanalyse_read:
        fanalyse_string = fanalyse_read.read().splitlines()

    for line_analyse in fanalyse_string:
        for line_dico in fdico_read:
            if re.search(line_dico, line_analyse):
                if line_dico == 's10':
                    listeDispo.append(nd1)
                    listeIntegr.append(ni2)
                    listeConfi.append(nc1)
                elif line_dico == 's15':
                    listeDispo.append(nd1)
                    listeIntegr.append(ni2)
                    listeConfi.append(nc2)
                elif line_dico == 's20':
                    listeDispo.append(nd2)
                    listeIntegr.append(ni1)
                    listeConfi.append(nc1)
                elif line_dico == 's25':
                    listeDispo.append(nd2)
                    listeIntegr.append(ni1)
                    listeConfi.append(nc1)
                elif line_dico == 's28':
                    listeDispo.append(nd3)
                    listeIntegr.append(ni2)
                    listeConfi.append(nc2)
                elif line_dico == 's30':
                    listeDispo.append(nd3)
                    listeIntegr.append(ni3)
                    listeConfi.append(nc3)

    for line_analyse in fanalyse_string:
        for line_dico in fdico_read:
            if re.search(line_dico, line_analyse):
                if line_dico == 's40':
                    listeDispo.append(nd4)
                    listeIntegr.append(ni4)
                    listeConfi.append(nc4)
                elif line_dico == 's50':
                    listeDispo.append(nd3)
                    listeIntegr.append(ni3)
                    listeConfi.append(nc3)
                elif line_dico == 's60':
                    listeDispo.append(nd3)
                    listeIntegr.append(ni4)
                    listeConfi.append(nc4)
                elif line_dico == 's70':
                    listeDispo.append(nd1)
                    listeIntegr.append(ni1)
                    listeConfi.append(nc1)
                elif line_dico == 's90':
                    listeDispo.append(nd1)
                    listeIntegr.append(ni1)
                    listeConfi.append(nc3)
                elif line_dico == 'sX9':
                    listeDispo.append(nd1)
                    listeIntegr.append(ni1)
                    listeConfi.append(nc2)

else:
    print("ERROR: Impossible de charger le fichier d'analyse")
    analyse = str(input("L'environnement le plus critique concerné [SXX] : "))
    analyse = analyse.lower()
    if analyse == 's10':
        listeDispo.append(nd1)
        listeIntegr.append(ni2)
        listeConfi.append(nc1)
    elif analyse == 's15':
        listeDispo.append(nd1)
        listeIntegr.append(ni2)
        listeConfi.append(nc2)
    elif analyse == 's20':
        listeDispo.append(nd2)
        listeIntegr.append(ni1)
        listeConfi.append(nc1)
    elif analyse == 's25':
        listeDispo.append(nd2)
        listeIntegr.append(ni1)
        listeConfi.append(nc1)
    elif analyse == 's28':
        listeDispo.append(nd3)
        listeIntegr.append(ni2)
        listeConfi.append(nc2)
    elif analyse == 's30':
        listeDispo.append(nd3)
        listeIntegr.append(ni3)
        listeConfi.append(nc3)
    elif analyse == 's40':
        listeDispo.append(nd4)
        listeIntegr.append(ni4)
        listeConfi.append(nc4)
    elif analyse == 's50':
        listeDispo.append(nd3)
        listeIntegr.append(ni3)
        listeConfi.append(nc3)
    elif analyse == 's60':
        listeDispo.append(nd3)
        listeIntegr.append(ni4)
        listeConfi.append(nc4)
    elif analyse == 's70':
        listeDispo.append(nd1)
        listeIntegr.append(ni1)
        listeConfi.append(nc1)
    elif analyse == 's90':
        listeDispo.append(nd1)
        listeIntegr.append(ni1)
        listeConfi.append(nc3)
    elif analyse == 'sX9':
        listeDispo.append(nd1)
        listeIntegr.append(ni1)
        listeConfi.append(nc2)

# Vérification de l'état de CID
urlpage = "https://cve-search.iicrai.org/cve/" + cveNumber
response = requests.get(urlpage)

if response.ok:
    soup = BeautifulSoup(response.text, 'lxml')
    stringCID = soup.find('table', {'class': 'table table-hover table-bordered cve-info'}).text
    list_one = [stringCID]
    list_one = [w.replace('\n', ',') for w in list_one]
    word_one = ''.join(list_one)
    word_one = word_one[1:-2]
    word_one = word_one.replace(",,", ",")
    list_two = word_one.split(',')
    lenList = len(list_two)
    failList = ['NONE', 'PARTIEL', 'COMPLETE', 'INCONNU']

    if lenList != 4:
        print("Pas d'indications concernant l'atteinte CID!")
        print("Veuillez sélectionner NONE[1], PARTIEL[2], COMPLETE[3] ou Inconnu[4]")
        conf_State_int = int(input("Confidentialité : "))
        conf_State_int -= 1
        integr_State_int = int(input("Intégrité : "))
        integr_State_int -= 1
        Dispo_State_int = int(input("Disponibilité : "))
        Dispo_State_int -= 1

        conf_State = failList[conf_State_int]
        integr_State = failList[integr_State_int]
        Dispo_State = failList[Dispo_State_int]
    elif lenList == 4:
        conf_State = list_two[1]
        integr_State = list_two[2]
        Dispo_State = list_two[3]
    else:
        print("Une erreur dans la lecture de l'indication est arrivé!")
        print("Le script continue...")
        conf_State = "ERREUR"
        integr_State = "ERREUR"
        Dispo_State = "ERREUR"

    print("La confidentialité est " + conf_State)
    print("L'intégrité est " + integr_State)
    print("La disponiblité est " + Dispo_State)

    if conf_State == "PARTIAL":
        listeConfi.append(nc2)
    elif conf_State == "COMPLETE":
        listeConfi.append(nc3)
    elif conf_State == "NONE":
        listeConfi.append(nc1)
    else:
        pass

    if integr_State == "PARTIAL":
        listeIntegr.append(nc2)
    elif integr_State == "COMPLETE":
        listeIntegr.append(nc3)
    elif integr_State == "NONE":
        listeIntegr.append(nc1)
    else:
        pass

    if Dispo_State == "PARTIAL":
        listeDispo.append(nc2)
    elif Dispo_State == "COMPLETE":
        listeDispo.append(nc3)
    elif Dispo_State == "NONE":
        listeDispo.append(nc1)
    else:
        pass

inoff = input("Asset(s) accessible(s) [D]istant, [A]djacent ou [L]ocal : ")
while True:
    if inoff == "D":
        listeConfi.append(nc4)
        listeDispo.append(nc4)
        listeIntegr.append(nc4)
        break
    elif inoff == "A":
        listeConfi.append(nc2)
        listeDispo.append(nc2)
        listeIntegr.append(nc2)
        break
    elif inoff == "L":
        listeConfi.append(nc1)
        listeDispo.append(nc1)
        listeIntegr.append(nc1)
        break
    else:
        print("Vous n'avez pas répondu à la question !")

# Questions / réponses
print("OK: Fin de l'analyse du score CID\n")
print("OK: Analyse de la vraissemblance")

urlpage = "https://nvd.nist.gov/vuln/detail/" + cveNumber
response = requests.get(urlpage)

# Infos cherché sur le site nvd.nist.gov
if response.ok:
    soup = BeautifulSoup(response.text, 'lxml')

    # Enregistrement du score CVS
    cveScore = soup.find('a', attrs={'class': 'label'})
    cveScore_txt = cveScore.text
    if cveScore_txt == "N/A":
        csvScore_float = float(input("CVS de la vulnérabilité : "))
    else:
        csvScore_list = re.findall("\d+\.\d+", cveScore_txt)
        csvScore_list = [str(integer) for integer in csvScore_list]
        csvScore_list = "".join(csvScore_list)
        csvScore_float = float(csvScore_list)
    if csvScore_float <= 4:
        listeVrais.append(nv1)
        print("Le score CVS est de : " + str(csvScore_float)
              + " pour un niveau de 1")
    elif csvScore_float <= 6:
        listeVrais.append(nv2)
        print("Le score CVS est de : " + str(csvScore_float)
              + " pour un niveau de 2")
    elif csvScore_float <= 8:
        listeVrais.append(nv3)
        print("Le score CVS est de : " + str(csvScore_float)
              + " pour un niveau de 3")
    elif csvScore_float <= 10:
        listeVrais.append(nv4)
        print("Le score CVS est de : " + str(csvScore_float)
              + " pour un niveau de 3")

    # Enregistrement du détail
    cveDetails = soup.find('p', attrs={'data-testid': 'vuln-description'})
    details = cveDetails.text
    details = details.strip()
    print("Le détail : " + '"' + details + '"')

# Si l'url ne trouve pas la CSV
else:
    while valint >= 10.1:
        val = input("Niveau de la CVE /10 ? : ")
        valint = float(val)
        if valint <= 4:
            listeVrais.append(nv1)
            break
        elif valint <= 6:
            listeVrais.append(nv2)
            break
        elif valint <= 8:
            listeVrais.append(nv3)
            break
        elif valint <= 10:
            listeVrais.append(nv3)
            break
        else:
            print("Vous n'avez pas répondu.")

    details = input("Détail de la vulnérabilité : ")
    details = details.strip()

# Info recherché sur https://www.security-database.com/
urlpage = "https://cve-search.iicrai.org/cve/" + cveNumber
response = requests.get(urlpage)

if response.ok:
    soup = BeautifulSoup(response.text, 'lxml')
    string = soup.find('table', class_='table table-hover table-bordered'
                       + ' cve-info table-even').text
    # Vecteur d'attaque
    if 'LOCAL' in string:
        listeVrais.append(nv1)
        print("Le vecteur d'attaque est local.")
        srcMenace_Vecteur = ", interne"
    elif 'NETWORK' in string:
        listeVrais.append(nv4)
        print("Le vecteur d'attaque est distant.")
        srcMenace_Vecteur = ", distant"
    elif 'ADJACENT_NETWORK' in string:
        listeVrais.append(nv2)
        print("Le vecteur d'attaque est adjacent.")
        srcMenace_Vecteur = ", adjacent"
    else:
        print("Impossible de trouver le vecteur d'attaque!")
        while True:
            val = input("Vulnérabilité Remote[1], Adjacent[2] ou local [3]?: ")
            if val == "1":
                listeVrais.append(nv4)
                print("Le vecteur d'attaque est distant.")
                srcMenace_Vecteur = ", distant"
                break
            elif val == "2":
                listeVrais.append(nv2)
                print("Le vecteur d'attaque est adjacent.")
                srcMenace_Vecteur = ", adjacent"
                break
            elif val == "3":
                listeVrais.append(nv1)
                print("Le vecteur d'attaque est local.")
                srcMenace_Vecteur = ", interne"
            else:
                print("Réponse non valide!")

    # Complexité de l'exploit
    if "LOW" in string:
        listeVrais.append(nv4)
        print("La compléxité d'exploit est faible.")
        srcMenace_Complex = " et avec de faibles capacités."
    elif "MEDIUM" in string:
        listeVrais.append(nv2)
        print("La compléxité d'exploit est moyenne.")
        srcMenace_Complex = " et avec des capacités importantes."
    elif "HIGH" in string:
        listeVrais.append(nv1)
        print("La compléxité d'exploit est élevé.")
        srcMenace_Complex = " et avec des capacités illimités."
    else:
        print("Impossible de trouver le niveau de complexité!")
        while True:
            val = input("Vulnérabilité remote LOW / MEDIUM / HIGH ? [L/M/H]: ")
            if val == "L":
                listeVrais.append(nv4)
                srcMenace_Complex = " et avec de faibles capacités."
                break
            elif val == "M":
                listeVrais.append(nv3)
                srcMenace_Complex = " et avec des capacités importantes."
                break
            elif val == "H":
                listeVrais.append(nv2)
                srcMenace_Complex = " et avec des capacités illimités."
                break
            else:
                print("Réponse non valide!")

    # Authentification
    if "NONE" in string:
        listeVrais.append(nv4)
        print("Aucune authentifications nécessaires.")
        srcMenace_Auth = ", sans authentifications"
    elif "SINGLE" in string:
        listeVrais.append(nv2)
        print("Authentification(s) simple(s) nécessaire(s).")
        srcMenace_Auth = ", avec une authentification simple"
    elif "MULTIPLE" in string:
        listeVrais.append(nv1)
        print("MFA nécessaire.")
        srcMenace_Auth = ", avec au moins plusieurs athentifications"
    else:
        print("Impossible de trouver les informations sur l'authentification!")
        while True:
            val = input("Vulnérabilité avec NONE / SINGLE /"
                        + " MULTIPLE ? [N/S/M]: ")
            if val == "N":
                listeVrais.append(nv4)
                srcMenace_Auth = ", sans authentifications"
                break
            elif val == "S":
                listeVrais.append(nv3)
                srcMenace_Auth = ", avec une authentification simple"
                break
            elif val == "M":
                listeVrais.append(nv2)
                srcMenace_Auth = ", avec au moins plusieurs athentifications"
                break
            else:
                print("Réponse non valide!")

    # Type d'exploits techniques
    cvx = 1
    str1 = " ; "
    list_exploit = []
    while string:
        try:
            string = soup.find('span', {'data-target': '#c' + str(cvx)}).text
            string = string.strip()
            list_exploit.append(string)
            cvx += 1
        except AttributeError:
            break
    exploitType = str1.join(list_exploit)

    if not exploitType:
        exploitType = "Précisions techniques sur l'exploit introuvable"
    else:
        print("Les exploits remontées sont : " + exploitType)

# Si le site est injoignable
else:
    while True:
        val = input("Vulnérabilité remote ? [O/N]: ")
        if val == "O":
            listeVrais.append(nv4)
            break
        elif val == "N":
            listeVrais.append(nv1)
            break
        else:
            print("Vous n'avez pas répondu!")

    while True:
        print("Niveau de compléxité de l'exploit")
        val = input("Faible, Moyenne(importante) ou illimité ? [F/M/I]: ")
        if val == "F":
            listeVrais.append(nv4)
            break
        elif val == "M":
            listeVrais.append(nv3)
            break
        elif val == "I":
            listeVrais.append(nv2)
            break
        else:
            print("Vous n'avez pas répondu!")

    while True:
        val = input("Accès Auth ? [O/N]: ")
        if val == "O":
            listeVrais.append(nv1)
            break
        elif val == "N":
            listeVrais.append(nv3)
            break
        else:
            print("Vous n'avez pas répondu.")

    while True:
        val = input("Un ou plusieurs supports"
                    + " exposés sur internet ? [O/N/I]: ")
        if val == "O":
            listeVrais.append(nv3)
            break
        elif val == "I":
            listeVrais.append(nv2)
            break
        elif val == "N":
            listeVrais.append(nv1)
            break
        else:
            print("Vous n'avez pas répondu.")

srcMenace_list = 1
print("\nLes sources de menaces :")
print('Source Humaine agissant de manière délibéré [1], Source Humaine '
      + 'agissant de manière accidentelle [2] ou Sources non humaines [3]')
while True:
    srcMenace = input("Sélectionnez une source de menace [1/2/3] : ")
    if srcMenace_list == 1:
        srcMenace_desc = 'Source Humaine agissant de manière délibéré'
        break
    elif srcMenace_list == 2:
        srcMenace_desc = 'Source Humaine agissant de manière accidentelle'
        break
    elif srcMenace_list == 3:
        srcMenace_desc = 'Sources non humaines'
        break
    else:
        print("Vous n'avez pas sélectionné de sources de menaces!")

srcMenace = srcMenace_desc + srcMenace_Vecteur + srcMenace_Auth + srcMenace_Complex

print("\nDéfinir le résumé d'exploit principal")
print("Dos [1], Code Execution [2], Overflow [3], Memory Corruption [4],"
      + " Sql Injection [5], Directory Traversal [6], Http Response"
      + " Splitting [7], Bypass something [8], Gain Information [9], Gain"
      + " Privileges [10], CSRF [11] ou File Inclusion [12]")

src_impact = ['Dos', 'Code Execution', 'Overflow', 'Memory Corruption',
              'Sql Injection', 'Directory Traversal', 'Http Response',
              'Splitting', 'Bypass something', 'Gain Information',
              'Gain Privileges', 'CSRF', 'File Inclusion']

while True:
    x = int(input("Choisissez dans la liste ci-dessus : "))
    if x >= 1 and x < 13:
        x -= 1
        input_stringList = src_impact[x]
        ExploitPrincType = str(input_stringList).strip('[]')
        break
    else:
        print("Vous n'avez pas sélectionné le type d'exploit principal!")

list_bien = ['MAT', 'LOG', 'RSX', 'PER', 'PAP', 'CAN']
print('\nLes biens: MAT, LOG, RSX, PER, PAP ou CAN')
while True:
    bien = input("Type de bien concerné ? : ")
    if bien in list_bien:
        break
    else:
        print("Vous n'avez pas renseigné un bien existant")

bien = bien.strip()
support = input("Type de bien support concerné ? : ")
support = support.strip()

# Moyenne + arrondis au plus proche entier
valeurConfi = statistics.mean(listeConfi)
arrondisConfi = valeurConfi
arrondisConfi = round(arrondisConfi)
arrondisConfi = int(arrondisConfi)

valeurIntegr = statistics.mean(listeIntegr)
arrondisIntegr = valeurIntegr
arrondisIntegr = round(arrondisIntegr)
arrondisIntegr = int(arrondisIntegr)

valeurDispo = statistics.mean(listeDispo)
arrondisDispo = valeurDispo
arrondisDispo = round(arrondisDispo)
arrondisDispo = int(arrondisDispo)

valeurVrais = statistics.mean(listeVrais)
arrondisVrais = valeurVrais
arrondisVrais = round(arrondisVrais)
arrondisVrais = int(arrondisVrais)

listNR = [arrondisConfi, arrondisIntegr, arrondisDispo]
niveauRisque = max(listNR) * arrondisVrais

while True:
    if os.path.exists(ftemp) is True:
        print("Le fichier dsv_analyse.csv est ouvert!")
        x = input("Faite ENTRER quand vous avez fermé le fichier.")
    else:
        break

# # Lier l'état et le niveau de CID
# Lier l'état et le niveau de confidentialité
if conf_State == "NONE":
    arrondisConfi = "Innaplicable"
elif conf_State == "PARTIAL" or conf_State == "COMPLETE":
    arrondisConfi = str(arrondisConfi)
else:
    print("Erreur dans la lecture de l'état confidentialité")

# Lier l'état et le niveau de Intégrité
if integr_State == "NONE":
    arrondisIntegr = "Innaplicable"
elif integr_State == "PARTIAL" or integr_State == "COMPLETE":
    arrondisIntegr = str(arrondisIntegr)
else:
    print("Erreur dans la lecture de l'état confidentialité")

# Lier l'état et le niveau de disponibilité
if Dispo_State == "NONE":
    arrondisDispo = "Innaplicable"
elif Dispo_State == "PARTIAL" or Dispo_State == "COMPLETE":
    arrondisDispo = str(arrondisDispo)
else:
    print("Erreur dans la lecture de l'état confidentialité")

print("OK: Fin de l'analyse de la vraissemblance")
# Impression des résultats
print("\n~~ Résultat de l'analyse initiale ~~")
print(eventName + " | " + fsvName + " | " + cveNumber)
print("Après analyse, la moyenne de la confidentialité des assets "
      + "analysés sont de " + str(arrondisConfi))
print("Après analyse, la moyenne de l'intégrité des assets "
      + "analysés sont de " + str(arrondisIntegr))
print("Après analyse, la moyenne de la disponibilité des assets "
      + "analysés sont de " + str(arrondisDispo) + "\n")
print("La vraissemblance est de " + str(arrondisVrais))
print("Le niveau de risque est de : [" + str(niveauRisque) + "]")

# Ajout au CSV d'analyse
fields = ["A traiter", eventName, fsvName, cveNumber, CVS, bien, support,
          ExploitPrincType, exploitType, srcMenace, "A décrire", "A décrire",
          conf_State, integr_State, Dispo_State, arrondisConfi, arrondisIntegr,
          arrondisDispo, arrondisVrais, "", "", "", "", details]

with open(fresultat, 'a', encoding='utf-8') as f:
    writer = csv.writer(f)
    writer.writerow(fields)

f.close()
