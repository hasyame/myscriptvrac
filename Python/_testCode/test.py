import os

print("Dossier à analyser : ")
walk_dir = str(input())

NumberOfLine = 0

print('walk_dir = ' + walk_dir)

print('walk_dir (absolute) = ' + os.path.abspath(walk_dir))

for root, subdirs, files in os.walk(walk_dir):
    list_file_path = os.path.join(root, 'my-directory-list.txt')

    with open(list_file_path, 'wb') as list_file:
        for filename in files:
            file_path = os.path.join(root, filename)

            with open(file_path, 'rb') as f:
                text = f.readlines()
                print("Line total for " + file_path + " : " + str(len(text)))
                NumberOfLine = NumberOfLine + len(text)

print(NumberOfLine)
