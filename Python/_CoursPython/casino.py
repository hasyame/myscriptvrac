#!/usr/bin/env python3
# Cours python
import random

nombre = 51
i = 1
x = 1
y = 1
continuerpartie = True

print("Vous rentrez dans le casino et vérifier combiens d'argent vous avez...")
while y == 1:
    try:
        argent = float(input("Vous possédez: "))
        break
    except ValueError:
        print("La valeur n'est pas bonne.")
        continue
print("\nUn croupier vous acceuil, l'air enjouer à l'idée que vous allez miser.")
print("'Bienvenue dans le casino et au jeu de la roue !' Déclare t'il.")
print("La partie commence...\n")

while continuerpartie:
    z = 1
    randnum = random.randint(1, 50)
    mise = 0
    while mise <= 0 or mise > argent:
        mise = input("Votre mise: ")
        try:
            mise = float(mise)
        except ValueError:
            print("Vous n'avez pas saisie de nombre !")
            mise = -1
            continue
        if mise <= 0:
            print("La mise est nulle ou négative!")
        if mise > argent:
            print("Vous n'avez pas tant d'argent! Vous avez " + str(argent)
                  + "€ sur vous.")

    while nombre >= 51 or nombre < 1:
        try:
            nombre = int(input("Donnez votre numéro entre 1 et 50: "))
        except ValueError:
            print("La valeur fournise n'est pas bonne.")
            continue

    print("Vous avez misé: " + str(mise) + "€")
    print("Vous avez choisis: " + str(nombre))
    print("La roue tourne... Et le numéro gagnant est le " + str(randnum) + "!")
    if nombre == randnum:
        mise = mise * 3
        print("Vous avez gagné " + str(mise) + "€")
        argent = argent + mise
    elif nombre % 2 == 0 and randnum % 2 == 0 or nombre % 2 != 0 and randnum % 2 != 0:
        mise = mise / 2
        print("Vos numéros sont de la même couleur, le croupier vous rends"
              + " la moitié de la mise!")
        print("Vous gardez " + str(mise) + "€")
        argent = argent + mise
    else:
        print("Vous avez perdu !")

    print("Dans votre porte monnaie vous avez: " + str(argent))

    continuer = input("Voulez-vous continuer ? [O/N]")
    i = 1
    x = 1
    y = 1
    z = 1
    while z == 1:
        if continuer == "O" or continuer == "o":
            continuerpartie = True
            break
        elif continuer == "N" or continuer == "n":
            continuerpartie = False
            break
        else:
            print("Vous n'avez pas renseigner de valeur juste")
            continue
