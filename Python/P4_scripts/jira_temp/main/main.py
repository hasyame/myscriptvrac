# python3
# Main script
from jira.client import JIRA
import logging
import os


def connect_jira(log, jira_server, jira_user, jira_password):
    os.environ['https_proxy'] = 'http://proxy.intranet-adsn.fr:8080/'
    os.environ['http_proxy'] = 'http://proxy.intranet-adsn.fr:8080/'
    try:
        log.info("Connecting to JIRA: %s", jira_server)
        jira_options = {
            'server': jira_server,
            'verify': False,
            'stream': True
            }

        jira = JIRA(
            options=jira_options,
            basic_auth=(jira_user, jira_password),
            validate=True,
            )

    except Exception:
        log.error("Failed to connect to JIRA")

    try:
        issue = jira.issue("IPT-10")
        print(issue.key + " + " + issue.fields.summary
              + " + " + issue.fields.reporter.displayName
              )
    except Exception:
        log.error("Failed to show informations")

    try:
        jira.add_worklog(issue,
                         started="2021-07-29",
                         timeSpent="2h"
                         )
    except Exception:
        log.error("Failed to add work")

    try:
        issue = jira.issue("IPT-10")
        print(issue.key + " + " + issue.fields.summary
              + " + " + issue.fields.reporter.displayName
              )
    except Exception:
        log.error("Failed to show informations")


url = input(str("URL: "))
user = input(str("Nom d'utilisateur: "))
passwd = input(str("Mot de passe: "))
# create logger
log = logging.getLogger(__name__)

# NOTE: You put your login details in the function call connect_jira(..) below!

# create a connection object, jc
jc = connect_jira(log, url, user, passwd)

# print names of all projects
# projects = jc.projects()
# for v in projects:
#    print(v)

print(jc)
