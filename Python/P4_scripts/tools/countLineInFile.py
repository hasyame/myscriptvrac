import os

walk_dir = input("Folder to analyse : ")

NumberOfLine = 0

for root, subdirs, files in os.walk(walk_dir):
    for filename in files:
        file_path = os.path.join(root, filename)

        with open(file_path, 'rb') as f:
            text = f.readlines()
            print("Line total for " + file_path + " : " + str(len(text)))
            NumberOfLine = NumberOfLine + len(text)

print("The total number of line is: " + str(NumberOfLine))
