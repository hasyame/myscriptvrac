# Dépendance
import os
import sys
import glob
import re
import argparse

# Script
def recursive():
    pass

def grep(fichier):
    print(fichier)

def main():
    import argparse

    parser = argparse.ArgumentParser(description='A test program.')
    parser.add_argument("-g", "--grep", help="Rerchercher un mot précis:" +
                        " -g 'toto'.")
    parser.add_argument("-d", "--dico", help="Dictionnaire pour rechercher" +
                        " un ensemble de strings: -d ~/documents/dico.txt")
    parser.add_argument("-sf", "--strict_file", help="Rechercher dans un" +
                        " fichier spécifique.")
    parser.add_argument("-f", "--folder", help="Rechercher dans tous les" +
                        " fichiers présents dans un dossier.")
    args = parser.parse_args()
    grepexec = args.grep
    dicoexec = args.dico

    if args.grep and args.dico or args.strict_file and args.folder:
        print("Vous ne pouvez pas utilisez ces deux arguments en même temps.")
    elif args.grep:
        grep(grepexec)
    elif args.dico:
        grep(dicoexec)
    else:
        print("Aucun arguments n'a été fournis, veuillez faire -h pour plus" +
              " d'informations.")

if __name__ == '__main__':
    main()
