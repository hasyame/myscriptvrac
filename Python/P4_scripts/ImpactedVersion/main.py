import requests
import csv
import os
from pathlib import Path
from bs4 import BeautifulSoup

# Variables
listGlobal = []


def csvAppend(fullPathName, value, cve_id, numTicket):
    with open(fullPathName, 'a', newline='') as fd:
        writer = csv.writer(fd)
        writer.writerow([numTicket, cve_id, value])


# Création du CSV
def csvGen(fullPathName):
    if os.path.isfile(fullPathName) is True:
        print("Fichier rapport existe.")
    else:
        with open(fullPathName, 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(["Ticket", "CVE ID", "OS-MIDDW impacté"])


# Fonction de parsing
def webParse(r, cve_id, numTicket):
    fullPathName = (str(Path.home()) + "\\Documents\\rapportCVEVers.csv")
    soup = BeautifulSoup(r.content, 'html.parser')
    soup_lines = soup.prettify().split('\n')

    csvGen(fullPathName)

    for line in soup_lines:
        line_striped = line.strip()
        if line_striped.startswith('*cpe') is True:
            listGlobal.append(line_striped)

    for brut_value in listGlobal:
        value = brut_value.replace("</pre>", "")
        csvAppend(fullPathName, value, cve_id, numTicket)


# Fonction d'exec principale
def main():
    cve_id = str(input("ID complet de la CVE: "))
    numTicket = str(input("ID Ticket: "))
    url_to_parse = ("https://nvd.nist.gov/vuln/detail/" + cve_id)

    # Making a GET request
    r = requests.get(url_to_parse)

    # Check si site joignable
    if int(r.status_code) == 200:
        webParse(r, cve_id, numTicket)
    else:
        print("Site non-joignable")


# Execution de la fonction principale
main()
