#!/usr/bin/env python3
import feedparser
import sys


def rss(editeur, middleware, count):
    entry_count = 1
    middleware = middleware.lower()
    url = "https://github.com/" + editeur + "/" + middleware + "/releases.atom"
    print("L'url consulté est la suivante: " + url)
    NewsFeed = feedparser.parse(url)
    while entry_count <= count:
        try:
            entry = NewsFeed.entries[entry_count]
            print(middleware + ": " + entry.title)
            entry_count = entry_count + 1
        except IndexError:
            print("Le projet, l'éditeur n'existe pas OU il n'y a pas de"
                  + " versions connues.")
            sys.exit()


def help():
    print("----------------------------------------")
    print("Le script interroge github pour connaitre la dernière version d'un"
          + " projet")
    print("Par défaut le script affiche la dernière version sortie")
    print("----------------------------------------")
    print("Usage: rsscheck.py -e <éditeur> -p <projet>"
          + " [options facultatifs...]")
    print("----------------------------------------")
    print("Obligatoire:")
    print("      -e, --editeur   Spécifier l'éditeur du projet à check.")
    print("      -p, --projet    Spécifier le projet.")
    print("Facultatif:")
    print("      -c, --count     Spécifier le nombre de versions à afficher"
          + " depuis la latest")
    print("      -c, --count     Affiche une liste d'éditeurs/projets")
    print("----------------------------------------")
    print("Exemple: rsscheck.py -e apache -p tomcat -> Affiche la dernière"
          + "version de tomcat")
    print("         rsscheck.py -l -> Affiche une liste d'éditeurs/projets")


def noargs():
    print("Aucun arguments, veillez à vous référer à l'aide: rsscheck.py -h")
    print("Exemple d'usage:  rsscheck.py -e apache -p tomcat")
