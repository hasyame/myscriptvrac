#!/usr/bin/env python3

import sys
import getopt
from functions import usage


# Fonction principale
def main():
    # Variables
    intCount = 1
    strEditeur = ""
    strProject = ""
    # liste des arguments
    argumentList = sys.argv[1:]
    # Options courtes
    options = "he:p:c:"
    # Options longues
    long_options = ["help", "editeur = ", "project = ", "count ="]

    # Execution des arguments
    try:
        arguments, values = getopt.getopt(argumentList, options, long_options)
        for currentArgument, currentValue in arguments:
            if currentArgument in ("-h", "--help"):
                usage.help()
                sys.exit()
            elif currentArgument in ("-e", "--editeur"):
                strEditeur = currentValue
            elif currentArgument in ("-p", "--project"):
                strProject = currentValue
            elif currentArgument in ("-c", "--count"):
                intCount = int(currentValue)

        if len(strEditeur) == 0 or len(strProject) == 0:
            usage.noargs()
            sys.exit()
        elif len(strEditeur) == 0 and len(strProject) == 0:
            print("Il manque un argument, veuillez consulter l'aide:"
                  + " -h, --help ")
            usage.help()
            sys.exit()
        else:
            usage.rss(strEditeur, strProject, intCount)

    # Erreurs output
    except getopt.error as err:
        print(str(err))


# Exec de la fonction principale
if __name__ == '__main__':
    main()
