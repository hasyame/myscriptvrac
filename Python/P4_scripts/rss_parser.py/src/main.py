# Parser feed RSS pour les versions des softs
# Package à installer: feedparser 6.0.2, mysql-connector 8.0.23
import feedparser
import mysql.connector as mysql
import xml.etree.ElementTree as ET

def main ():
    github_feed()

# Fonction des feeds github
def github_feed():
    try:
        feed_rss = open("../files/feed_git_rss.txt","r")
        feed_rss_list = feed_rss.readlines()
        feed_rss_list = [x[:-1] for x in feed_rss_list]
    except FileNotFoundError:
        print("Erreur fichier introuvable: veuillez fournir un fichier"
              + "feed_git_rss.txt dans le dossier 'files' du projet")
        exit()
    except ValueError:
        print("Erreur de syntaxe")
        exit()

    for item in feed_rss_list:
        news_feed = feedparser.parse(item)
        entry = news_feed.entries[1]
        lien = str(entry.link)
        app = str(split(lien))
        vers = str(entry.title)
        xml_db(app, vers)
        print("La version de " + app + " est " + vers)

    def xml_db(app, vers):
        usrconfig = ET.Element("Application")
        usrconfig = ET.SubElement(usrconfig, "Application")

        # insert list element into sub elements
        add_vers = ET.SubElement(Application, "version")
        usr.text = str(vers)

        tree = ET.ElementTree(usrconfig)

        # write the tree into an XML file
        tree.write("Output.xml", encoding ='utf-8', xml_declaration = True)

# Function de split pour récupérer le nom de l'application
def split (lien):
    list_lien = lien.split("/")
    nom_app = list_lien[4]
    return str(nom_app)

main()
