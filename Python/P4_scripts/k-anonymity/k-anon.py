# BREUL Benoît
# Pour rappel, k-anonymity est un principe simple d'anonymisation basé sur deux
# principes simples: La suppression ou la généralisation.
# Librairies
import pandas as pd
import warnings
import xlrd
import random

# Variables globales
fichier = 1
titles_list = []
len_valmax = 0

def generalisation(titles, df):
    lines_max = 0
    a = 1
    while a == 1:
        repl = str(input("La valeur est-elle un entier simple (âge,"
                         + " métrique, stats...)? [O/N]: "))
        if repl == "O":
            print("Généralisation en cours...")
            lines = int(len(df.axes[0]))
            while lines_max != lines:
                val = df.iloc[lines_max][titles]
                try:
                    val_float = float(val)
                except ValueError:
                    pass
                if isinstance(val_float, float):
                    rand = random.uniform(0.70, 0.85)
                    result = (val_float - (val_float * rand))
                    result_plus = val_float + result
                    result_plus = round(result_plus)
                    result_moins = val_float - result
                    result_moins = round(result_moins)
                    result_str = str("" + str(result_moins) + " - " + str(result_plus))
                    df.loc[lines_max,titles]=result_str
                else:
                    pass
                print("Ligne " + str(lines_max) + "/" + str(lines) + " traitées")
                lines_max = lines_max + 1
            print(df)
            break
        elif repl == "N":
            repl = str(input("Une date? [O/N]"))
            if repl == "O":
                print("Généralisation en cours...")
                break
            elif repl == "N":
                repl = str(input("ok"))
                break
            else:
                pass
        else:
            print("Mauvaise réponse...")

        print(df.iloc[2][titles])
        print(df)
        pass

def suppression(titles, df):
    a = 1
    b = 1
    print("Modification de la colonne " + titles)
    while a == 1:
        repl = str(input("Remplacer entièrement une valeur par une"
                         + " autre? [O/N] "))
        if repl == "O":
            anon = str(input("Valeur d'anonymisation: "))
            df[titles]=anon
            break
        elif repl == "N":
            print("Les valeurs seront remplacés par *")
            anon = "*"
            df[titles]=anon
            break
        else:
            print("Mauvaise réponse...")

    print(df)
    pass

def nonmodif(titles):
    print("Aucunes modifications pour " + titles)

def main(titles, df):
    # Variables
    x = 1
    case = "X"
    while x == 1:
        gs = str(input("Anonymisation par généralisation, suppression ou"
                       + "non modifications pour " + titles + " [G/S/M]: "))
        if gs == 'G':
            generalisation(titles, df)
            break
        elif gs == 'S':
            suppression(titles, df)
            break
        elif gs == 'M':
            nonmodif(titles)
            break
        else:
            print("Vous vous êtes trompé dans votre choix. Recommencez...")
            x = 1

if __name__ == '__main__':
    print("Ce script obéit au plus simple à l'anonymisation k-anonymity."
          + " Généralisation, suppression et non-modification.")
    while fichier == 1:
        fichier = str(input('Chemin complet du fichier csv/xlsx à anonymiser: '))
        try:
            with open(fichier, "r") :
                print(fichier + " est lisible.")
                with warnings.catch_warnings(record=True):
                    warnings.simplefilter("always")
                    df = pd.read_excel(fichier, engine="openpyxl")
                    print(df)
                    column_names = df.columns
                for titles in column_names:
                    titles_list.append(titles)
                len_list = len(titles_list)
                while len_valmax != len_list:
                    titles_str = str(titles_list[len_valmax])
                    main(titles_str, df)
                    len_valmax = len_valmax + 1
                save = str(input("Chemin complet du fichier anonymisé: "))
                df.to_excel(save, sheet_name='anonimized_sheet')
        except IOError:
            print("Erreur! Le fichier n'existe pas. Recommencez...")
            fichier = 1
