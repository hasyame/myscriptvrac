import json
#import mysql.connector

#mydb = mysql.connector.connect(
#  host="localhost",
#  user="cvesin",
#  password="pass2020",
#  database="cvesin"
#)

i = 0
x = 0
y = 0

jsonF = 'cve2002.json'

# Opening JSON file and loading the data
# into the variable data
file = open(jsonF, "r")
data = file.read()
occurrences = int(data.count("data_type"))
occurrences = occurrences - 1

with open(jsonF) as json_file:
    data = json.load(json_file)

while(i != occurrences):
    print("~~~~~~~~~~~~ " + str(i + 1) + "~~~~~~~~~~~~")
    try:
        dataType = data['CVE_Items'][i]['cve']['data_type']
        print(dataType)
    except KeyError:
        pass

    try:
        ID = data['CVE_Items'][i]['cve']['CVE_data_meta']['ID']
        print(ID)
    except KeyError:
        pass

    print(ID)
    while(x != 20):
        try:
            descpb= data['CVE_Items'][i]['cve']['problemtype']['problemtype_data'][0]['description'][x]['value']
            print(descpb)
            x = x + 1
            break
        except IndexError:
            x = 0
            break
    while(x != 20):
        try:
            urldata = data['CVE_Items'][i]['cve']['references']['reference_data'][x]['url']
            refdata = data['CVE_Items'][i]['cve']['references']['reference_data'][x]['refsource']
            #sql = "INSERT INTO refCVE (ID, urldata, refdata)"
            print(urldata + " | " + refdata)
            x = x + 1
        except IndexError:
            x = 0
            break
    while(x != 20):
        try:
            description = data['CVE_Items'][i]['cve']['description']['description_data'][x]['value']
            print(description)
            x = x + 1
        except IndexError:
            x = 0
            break
    while(x != 100):
        try:
            cpe = data['CVE_Items'][i]['configurations']['nodes'][0]['cpe_match'][x]['cpe23Uri']
            #sql = "INSERT INTO cpeCVE (id, cpe)"
            print(cpe)
            x = x + 1
        except KeyError:
            x = 0
            try:
                while(y != 10):
                    try:
                        while(x != 100):
                            try:
                                cpe = data['CVE_Items'][i]['configurations']['nodes'][0]['children'][y]['cpe_match'][x]['cpe23Uri']
                                #sql = "INSERT INTO refCVE (id, cpe)"
                                print(cpe)
                                x = x + 1
                            except IndexError:
                                x = 0
                                y = y + 1
                                break
                    except IndexError:
                        y = 0
                        break
                while(x != 100):
                    try:
                        cpe = data['CVE_Items'][i]['configurations']['nodes'][1]['cpe_match'][x]['cpe23Uri']
                        #sql = "INSERT INTO refCVE (id, cpe)"
                        x = x + 1
                        print(cpe)
                    except KeyError:
                        x = 100
                        break
                    except IndexError:
                        x = 100
                        break
            except IndexError:
                x = 0
                y = 0
                break
        except IndexError:
            x = 0
            break
    try:
        accessVector = data['CVE_Items'][i]['impact']['baseMetricV2']['cvssV2']['accessVector']
    except KeyError:
        accessVector = " "
    try:
        accessComplexity = data['CVE_Items'][i]['impact']['baseMetricV2']['cvssV2']['accessComplexity']
    except KeyError:
        accessComplexity = " "
    try:
        authentication = data['CVE_Items'][i]['impact']['baseMetricV2']['cvssV2']['authentication']
    except KeyError:
        authentication = " "
    try:
        confidentialityImpact = data['CVE_Items'][i]['impact']['baseMetricV2']['cvssV2']['confidentialityImpact']
    except KeyError:
        confidentialityImpact = " "
    try:
        integrityImpact = data['CVE_Items'][i]['impact']['baseMetricV2']['cvssV2']['integrityImpact']
    except:
        integrityImpact = " "
    try:
        availabilityImpact = data['CVE_Items'][i]['impact']['baseMetricV2']['cvssV2']['availabilityImpact']
    except KeyError:
        availabilityImpact = " "
    try:
        baseScore = data['CVE_Items'][i]['impact']['baseMetricV2']['cvssV2']['baseScore']
        print(baseScore)
    except KeyError:
        baseScore = " "
    try:
        severity = data['CVE_Items'][i]['impact']['baseMetricV2']['severity']
    except KeyError:
        severity = " "
    try:
        exploitabilityScore = data['CVE_Items'][i]['impact']['baseMetricV2']['exploitabilityScore']
    except KeyError:
        exploitabilityScore = " "
    try:
        impactScore = data['CVE_Items'][i]['impact']['baseMetricV2']['impactScore']
    except KeyError:
        impactScore = " "
    try:
        obtainAllPrivilege = data['CVE_Items'][i]['impact']['baseMetricV2']['obtainAllPrivilege']
    except KeyError:
        obtainAllPrivilege = " "
    try:
        obtainUserPrivilege = data['CVE_Items'][i]['impact']['baseMetricV2']['obtainUserPrivilege']
    except KeyError:
        obtainUserPrivilege = " "
    try:
        obtainOtherPrivilege = data['CVE_Items'][i]['impact']['baseMetricV2']['obtainOtherPrivilege']
        print(obtainOtherPrivilege)
    except KeyError:
        obtainOtherPrivilege = " "
    try:
        userInteractionRequired = data['CVE_Items'][i]['impact']['baseMetricV2']['obtainOtherPrivilege']
    except KeyError:
        userInteractionRequired = " "
    try:
        publishedDate = data['CVE_Items'][i]['publishedDate']
        print(publishedDate)
    except KeyError:
        publishedDate = " "
    try:
        lastModifiedDate = data['CVE_Items'][i]['lastModifiedDate']
        print(lastModifiedDate)
    except KeyError:
        lastModifiedDate = " "

    #sql = "INSERT INTO CVE (type, id, descpb, urldata, refdata, description, accessVector, accessComplexity, authentication, confidentialityImpact, integrityImpact, availabilityImpact, baseScore, severity, exploitabilityScore, impactScore, obtainAllPrivilege, obtainUserPrivilege, obtainOtherPrivilege, userInteractionRequired, publishedDate, lastModifiedDate) VALUES (dataType, ID, descpb, urldata, refdata, description, accessVector, accessComplexity, authentication, confidentialityImpact, integrityImpact, availabilityImpact, baseScore, severity, exploitabilityScore, impactScore, obtainAllPrivilege, obtainUserPrivilege, obtainOtherPrivilege, userInteractionRequired, publishedDate, lastModifiedDate)"
    i = i + 1
