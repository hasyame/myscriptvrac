# Fonction principale du script
def main():
    valueok = int(0)
    while valueok == 0:
        taille_str = input("Veuillez saisir votre taille en cm: ")
        try :
            taille_int = int(taille_str)
            break
        except ValueError:
            print("Veuillez founir un entier.")

    while valueok == 0:
        poids_str = input("Veuillez saisir votre poids en kg: ")
        try :
            poids_flt = float(poids_str)
            break
        except ValueError:
            print("Veuillez founir une valeur décimale.")

    taille_int = taille_int / 100
    taille_pow = pow(taille_int,2)
    print("Votre IMC est : " + str(imc_calc(taille_pow, poids_flt)))
    imc_comment(imc_calc(taille_pow, poids_flt))

# Fonction de calcul
def imc_calc(taille, poids):
    imc = float(poids) / taille
    imc = round(imc,2)
    return float(imc)

# Fonction de commentaire de l'IMC
def imc_comment(imc_flt):
    if imc_flt < 18.5:
        pointplus = 18.5 - imc_flt
        pointplus = round(pointplus,2)
        print("Vous êtes trop maigre.")
        print("Vous êtes à " + str(pointplus) + " de la corpulence normale.")
    elif 18.5 < imc_flt < 24.9:
        pointmoins = imc_flt - 18.5
        pointplus = 24.9 - imc_flt
        pointmoins = round(pointmoins,2)
        pointplus = round(pointplus,2)
        print("Vous avez une corpulence normale.")
        print("Vous êtes à " + str(pointplus) + " du surpoids et "
              + str(pointmoins) + " de la maigreur.")
    elif 25 < imc_flt < 29.9:
        pointplus = 25 - imc_flt
        pointplus = round(pointplus,2)
        print("Vous êtes en surpoids.")
        print("Vous êtes à " + str(pointplus) + " de la corpulence normale.")
    elif imc_flt > 30:
        return print("Vous êtes en obésité.")
    else:
        return print("Une erreur est survenu dans le status de votre corpulence")

main()
