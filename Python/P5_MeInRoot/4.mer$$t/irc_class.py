import socket
import sys
import time

class IRC:

    irc = socket.socket()

    def __init__(self):
        # Attribution du socket
        self.irc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def sendPrivateMsg(self, username, msg):
        # Message privé
        self.irc.send(str.encode("PRIVMSG " + username + " :" + msg + "\n"))

    def send(self, channel, msg):
        # Channel message
        self.irc.send(bytes("PRIVMSG " + channel + " " + msg + "\n", "UTF-8"))

    def connect(self, server, port, channel, botnick, botpass, botnickpass):
        # Connexion au serveur
        print("Connecting to: " + server)
        self.irc.connect((server, port))

        # Authentification
        self.irc.send(bytes("USER " + botnick + " " + botnick +" " + botnick + " :python\n", "UTF-8"))
        self.irc.send(bytes("NICK " + botnick + "\n", "UTF-8"))
        self.irc.send(bytes("NICKSERV IDENTIFY " + botnickpass + " " + botpass + "\n", "UTF-8"))
        time.sleep(5)

        # Rejoindre un channel
        self.irc.send(bytes("JOIN " + channel + "\n", "UTF-8"))

    def get_response(self):
        time.sleep(0.2) # A modifier selon la latence du srv 1 est reco
        # Capture de la réponse
        resp = self.irc.recv(2040).decode("UTF-8")
        data = resp[resp.find(' :'):]
        data = (data.replace(' :', ''))

        if resp.find('PING') != -1:
            self.irc.send(bytes('PONG ' + resp.split() [1] + '\r\n', "UTF-8"))

        return data
