#!/usr/bin/env python3
# Script permettant de rechercher dans un dictionnaire des mots pour les jeux
# les jeux de données
import os
import re
import sys
import time
import threading

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Mutex pour multiprocessing


# mutex = Lock()
maxthreads = 4
sema = threading.Semaphore(value=maxthreads)
threads = list()

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Fonction de formatage en poids lisibles des fichiers


def convert_bytes(num):  # "Human Readable" pour les fichiers
    for x in ['bytes', 'KB', 'MB', 'GB', 'TB']:
        if num < 1024.0:
            return "%3.1f %s" % (num, x)
        num /= 1024.0


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Fonction de calcul de temps d'analyse par fichiers


def file_size(file_path):  # Conversion du poids des fichiers
    if os.path.isfile(file_path):
        statinfo = os.stat(file_path)
        real_size = int(statinfo.st_size)
    if real_size <= 4096:
        print("Temps d'analyse estimé à moins de 30 secondes")
        print('--------------------------------------------------------------')
    elif real_size <= 16384:
        print("Temps d'analyse estimé à moins de 1 minute")
        print('--------------------------------------------------------------')
    elif real_size <= 65536:
        print("Temps d'analyse estimé à moins de 2 minutes")
        print('--------------------------------------------------------------')
    elif real_size <= 262144:
        print("Temps d'analyse estimé à moins de 4 minutes")
        print('--------------------------------------------------------------')
    elif real_size <= 1048576:
        print("Temps d'analyse estimé à moins de 8 minutes")
        print('--------------------------------------------------------------')
    elif real_size > 1048576:
        print("Temps d'analyse estimé à plus de 10 minutes")
        print('--------------------------------------------------------------')

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Fonction de grep pour les fichiers .sql


def grepi_sql(dico, fichier):
    line_number = 0
    risque_enum = 0
    nameFile = os.path.basename(fichier)
    result_final = []

    dicolist = open(dico).read().splitlines()

    with open(fichier) as ficlist:
        ficstring = ficlist.read().splitlines()

        fichier_open = str(fichier)
        with open('resultat.txt', 'a') as resultat:
            resultat.write(str(fichier_open) + "\n")

    for line in ficstring:
        ptrn = re.compile(r"\w*(" + "|".join(dicolist) + r")\b\w*",
                          flags=re.I)
        ptrn_result = ptrn.findall(line)
        if ptrn_result:
            line = line.write(' '.join(line.split()))
            result_final = (str(nameFile) + "`" + str(line_number) + "`"
                            + str(line) + "`" + str(ptrn.findall(line)))
            risque_enum = risque_enum + 1

            print(result_final)
            with open('resultat.txt', 'a') as resultat:
                resultat.write(str(result_final) + "\n")
        line_number += 1

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Fonction de grep pour les fichiers lisibles


def grepi(dico, fichier, result_location):
    sema.acquire()
    line_number = 0
    riskEnum = 0
    nameFile = os.path.basename(fichier)
    result_final = []
    start_time = time.time()

    dicolist = open(dico).read().splitlines()

    with open(fichier) as ficlist:
        ficstring = ficlist.read().splitlines()

        fichier_open = str(fichier)
        with open(result_location, 'a') as resultat:
            resultat.write("~---------------------------------------------~\n")
            resultat.write("Fichier: " + str(fichier_open) + "\n")
            resultat.close()

        try:
            for line in ficstring:
                ptrn = re.compile(r"\w*(" + "|".join(dicolist) + r")\b\w*",
                                  flags=re.I)
            ptrn_result = ptrn.findall(line)
        except ValueError:
            pass
            if ptrn_result:
                result_final = (str(nameFile) + "`" + str(line_number) + "`"
                                + str(' '.join(line.split())) + "`"
                                + str(ptrn.findall(line)))
                riskEnum = riskEnum + 1

                print(result_final)
                with open(result_location, 'a') as resultat:
                    resultat.write(str(result_final) + "\n")
                    resultat.close()
            else:
                pourcentage = (line_number / len(open(fichier).readlines()))\
                 * 100
                pourcentage = round(pourcentage, 2)
                time_end = (time.time() - start_time)
                print("Pourcentage d'analyse du fichier: " + str(pourcentage)
                      + "%"
                      + " | ligne " + str(line_number) + "/"
                      + str(len(open(fichier).readlines()))
                      + " | Temps écoulé: "
                      + str(round(time_end)) + " secondes")

            line_number += 1
    print("Nombre de lignes à risques énumérées total: " + str(riskEnum))
    sema.release()

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Fonction d'index des fichiers de manière récursives
# Creer et nomme le fichier qui acceuillera les résultats


def main(folder, generalData):
    lenTOTAL = 0
    timestr = time.strftime("%Y%m%d-%H%M%S")
    nameResultFile = timestr + "-ResultData.txt"
    nameErrorFile = timestr + "-Error.log"
    resultat = open(nameErrorFile, "a")
    resultat.close()
    resultat = open(nameResultFile, "a")
    resultat.close()
    result_location = nameResultFile
    starttime = time.time()

    # Nom des dictionnaires
    sqlData = "dico_sql.txt"

    # Affiche le résultat final et l'inscris dans le fichier ResultData.txt
    pathname = os.path.dirname(sys.argv[0])  # Récupère la position du script
    directory = pathname + r'\\' + folder

    start_num_files = sum([len(files) for r, d, files in os.walk(directory)])
    num_files = sum([len(files) for r, d, files in os.walk(directory)])
    print("Le nombre de fichier est de " + str(num_files))

    for root, directories, filenames in \
            os.walk(directory):
        for filename in filenames:
            completeFilePath = os.path.abspath(root + '\\' + filename)
            print("Analyse de : " + completeFilePath)
            file_size(completeFilePath)
            extension = completeFilePath.split('.')[-1]

            try:
                addlen = len(open(completeFilePath).readlines())
                lenTOTAL = addlen + lenTOTAL
            except ValueError:
                pass

            try:
                if extension == "sql":
                    t = threading.Thread(target=grepi, args=(sqlData,
                                                             completeFilePath,
                                                             result_location))
                    t.start()
                    # grepi(sqlData, completeFilePath, result_location)
                    os.system('cls' if os.name == 'nt' else 'clear')
                    num_files = num_files - 1
                    print("Fichier restant: " + str(num_files) + "\n")
                else:
                    t = threading.Thread(target=grepi, args=(sqlData,
                                                             completeFilePath,
                                                             result_location))
                    threads.append(t)
                    t.start()
                    os.system('cls' if os.name == 'nt' else 'clear')
                    num_files = num_files - 1
                    print("Fichier restant: " + str(num_files) + "/"
                          + str(start_num_files) + "\n")
            except ValueError:
                error = open(nameErrorFile, "a")
                print("/!\\ Fichier " + completeFilePath + " illisible /!\\")
                print("~---------------------------------------------~")
                error.write("Fichier " + completeFilePath + " illisible.\n")
                error.close()
                pass

    # Afficher le nombre de résultat
    len_result = open(result_location, 'r')
    len_text = len_result.readlines()
    nbr_len_text = len(len_text)
    print('~~ CTRL_Jeux de données ~~')
    print('Recherche de données de production...')
    print("Le nombre d'entrée suspecte est de : " + str(nbr_len_text) + "/"
          + str(lenTOTAL))
    print("Durée d'éxecution : {} seconds".format(time.time() - starttime))

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Fonction de boucle + écriture fichier + affichage résultat


if __name__ == '__main__':
    print('~~ CTRL_Jeux de données ~~\n')
    print('Recherche de données sensibles et/ou productions...\n')
    pathname = os.path.dirname(sys.argv[0])  # Récupère la position du script
    print("Enumération des fichiers et dossiers: " + str(os.listdir(pathname)))
    folder = str(input("Dossier contenant les données: "))
    generalData = str(input("Dictionnaire: "))
    main(folder, generalData)
