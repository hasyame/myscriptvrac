#!/usr/bin/env python3
import platform
import subprocess
import os
import socket
import time
import re


def ping(host, nbping):
    FNULL = open(os.devnull, 'w')
    param = '-n' if platform.system().lower() == 'windows' else '-c'
    command = ['ping', param, nbping, host]
    if subprocess.call(command, stdout=FNULL, stderr=subprocess.STDOUT) == 0:
        print("L'hôte est dispo, lets enum boy!")
    else:
        exit()


def isOpen(ip, port):
    print("\n")
    for p in port:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setblocking(0)
        sock.settimeout(0.15)
        protocolname = "tcp"
        try:
            sock.connect((ip, int(p)))
            protodet = sock.getservbyport(p, protocolname)
            print("Port " + str(p) + " => " + protodet)
            if p == 80:
                pass
            sock.shutdown(2)
            time.sleep(1)
            sock.close()
        except ValueError:
            print("Le serveur m'a jeté!")
            sock.close()
        except TimeoutError:
            print("Timeout dépassé pour " + str(p))
            pass
        except socket.timeout:
            print("Timeout dépassé pour " + str(p))
            pass
        except ConnectionRefusedError:
            pass


def http():
    pass


def main():
    while True:
        choice = str(input("IP ou URL: "))
        choice = choice.upper()
        if choice == "IP":
            ip = str(input("L'ip à scan: "))
            reg = re.compile("^([0-9]{1,3}\.){3}[0-9]{1,3}(:[0-9]{1,5})?$")
            if reg.match(ip):
                nbping = int(input("Nombre de pings: "))
                nbping = str(nbping)
                ping(ip, nbping)
                break
            else:
                print("Tu es sûr d'avoir fournis une IP?")
                continue
            break
        elif choice == "URL":
            url = str(input("URL: "))
            ip = socket.gethostbyname(url)
            print("J'ai trouvé cette ip: " + ip)
            nbping = int(input("Nombre de pings: "))
            nbping = str(nbping)
            ping(ip, nbping)
            break
        else:
            print("J'crois que tu t'es gouré, recommence.")
            continue

    port = []
    portsScan = str(input("Scan Global ou scan Discret [G/D]: "))
    if portsScan == "G":
        a = int(input("Port du début de la range: "))
        b = int(input("Port de fin de la range: "))
        b = b + 1
        port = [*range(a, b)]
        print(port)
    elif portsScan == "D":
        padd = int(input("Combiens de ports à scans: "))
        for i in range(0, padd):
            ele = int(input("Port: "))
            port.append(ele)
            print("Port " + str(ele) + " ajouté!")
    isOpen(ip, port)


if __name__ == '__main__':
    print("Bienvenue sur le script d'enum de ports discret!")
    main()
