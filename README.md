_!Scripts comment in french!_

**Description:** A bundle of scripts write for fun, training or simple daily usage.

**Language:** Rust, Perl, Python, SHELL, Powershell and Go.

**Python**
- Functions;
- Script grep confidential data;
- Courses python;
- Script enum and scoring CVE;
- Bundle of toolshack;
- Challenge.

**SHELL**
- Firewall conf n' scripts.

**Rust**
- RustBook work learning.

**Perl**
_Nothing to see here_

**Go**
_Nothing to see here_
