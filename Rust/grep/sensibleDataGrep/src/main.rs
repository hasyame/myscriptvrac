use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use std::io::BufRead;

fn main() {
    // variable chemin path vers le fichier + variable display affichage
    println!("Quel est le chemin complet ?");
    let mut chemin_string = String::new();
    let path = Path::new(&mut chemin_string);
    let display = path.display();

    // ouverture du chemin en read_only
    //let mut file = match File::open(&path) {
        //Err(why) => panic!("Ne peut pas ouvrir {}: {}", display, why),
        //Ok(file) => file,
    //};
    println!("{}", display);

    //let file = File::open(path.to_str());
    //let mut buf_reader = BufReader::new(file);
    //let mut contents = String::new();
    //buf_reader.read_to_string(&mut contents)?;
    //assert_eq!(contents, "Hello, world!");
    //Ok(())

    // lecture du fichier
    //let mut s = String::new();
    //let thomas: String = String::from("thomas");
    //match file.read_to_string(&mut s) {
        //Err(why) => panic!("Ne peut pas ouvrir {}: {}", display, why),
        //Ok(_) => print!("{} contient:\n{}", display, s),
    //};
    //}
}
