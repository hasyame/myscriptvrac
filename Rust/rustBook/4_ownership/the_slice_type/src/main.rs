fn main() {
    let the_string = String::from("top");
    let word = first_word(&the_string);
    let mut the_string = String::from("top");
    let test = &mut the_string;
    let octets = test.as_bytes();
    let lenght_bytes = octets.len();
    let lenght_test = test.len();
    println!("{:?} de longeur {} et string de longeur {} pour une valeur '{}'!", octets,
    lenght_bytes, lenght_test, word);
    //println!("{}", first_word(&mut the_string));
}

fn first_word(s: &String) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }

    &s[..]
}
