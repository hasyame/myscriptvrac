fn main() {
    let mut s = String::from("hello");
    change(&mut s);

    let mut s = String::from("hello there");

    {
        let r1 = &mut s;
        println!("Kenobi: {}", r1);
    } // r1 goes out of scope here, so we can make a new reference with no problems.

    let r2 = &mut s;
    println!("Grevious: OOh General Kenobi, {} to you too!", r2);

    let mut s = String::from("Yo");

    {
        let r1 = &s; // no problem
        let r2 = &s; // no problem
        println!("{} and {}", r1, r2);
        // r1 and r2 are no longer used after this point
    }
    println!("And more {}", r2);
    let r3 = &mut s; // no problem
    println!("{}", r3);
    println!("{}", no_dangle());
}

fn change(some_string: &mut String) {
    some_string.push_str(", world");
}

fn no_dangle() -> String {
    let s = String::from("hello");

    return s
}
