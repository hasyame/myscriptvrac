fn main() { // s is not valid here, it’s not yet declared

    let mut s = String::from("hello"); // s is valid from this point forward
    s.push_str(", world!"); // push_str() appends a literal to a String
    println!("{}", s); // This will print `hello, world!`

    let x = 5;
    let y = x; // Equal to x on the stack

    let s1 = String::from("hello");
    // let s2 = s1; -> Parceque ce n'est pas un "mutable", la pile considère s1 et s2 avec le même
    // n° index de pointeur (ptr); la même longueur (5) et la même capacité (5), naturellement Rust
    // supprime s1 de la pile pour éviter le risque de corruption de mémoire

    // println!("{}, world!", s1); // test de print s1 -> Ne fonctionnera pas

    let s2 = s1.clone(); // clonage en profondeur de la variable s1
    println!("s1 = {}, s2 = {}", s1, s2);

    println!("x = {}, y = {}", x, y);
} // this scope is now over, and s, s1 & s2 is no longer valid
