fn main() {
    let s = String::from("hello");  // s entre dans la portée sur la pile

    takes_ownership(s);             // la valeur s bouge sur la fonction
                                    // ... et n'est plus valide ici (hors de portée)

    let x = 5;                      // x entre dans la portée sur la pile

    makes_copy(x);                  // x devrait bouger comme s,
                                    // mais les valeurs i (32bits ici) sont des Copy donc fonctionne encore
                                    // x peut être encore appelé

} // Ici x sort de la portée et est drop, puis s. Mais s ayant bougé rien ne se passe.

fn takes_ownership(some_string: String) { // some_string entre dans la portée
    println!("{}", some_string);
} // Ici, some_string sort de la portée et est drop par Rust, la mémoire est libéré.

fn makes_copy(some_integer: i32) { // some_integer entre dans la portée
    println!("{}", some_integer);
} // Ici, some_integer sort de la portée. Rien de spécial n'arrive.
