#[derive(Debug)]
enum Gender {
    Male,
    Female
}

#[derive(Debug)]
struct Player {
    name: String,
    gender: Gender
}

fn main() {
    // init structs
    let player_1 = Player {
        name: String::from("Gordon Freeman"),
        gender: Gender::Male
    };

    let player_2 = Player {
        name: String::from("Alex Vance"),
        gender: Gender::Female
    };

    println!("Player 1: {:#?}", player_1);
    println!("Player 2: {:#?}", player_2);
}
