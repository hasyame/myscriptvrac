use std::io::{self, Write};

fn main() {
    let mut nombre_string = String::new();
    print!("Votre nombre: ");
    let _ = io::stdout().flush();
    io::stdin()
        .read_line(&mut nombre_string)
        .expect("\nFailed to read line");
    let nombre_int: i32 = nombre_string.trim().parse().unwrap();
    fib(nombre_int);

    println!("Résultat: {}", fib(nombre_int));
    println!("Tapez une touche pour terminer le programme.");
    let mut the_end = String::new();
    io::stdin()
        .read_line(&mut the_end)
        .expect("Failed");

}

fn fib(n : i32) -> i32 {
    if n <= 1 {
        return n;
    } else {
        return fib(n-1) + fib(n-2);
    }
}
