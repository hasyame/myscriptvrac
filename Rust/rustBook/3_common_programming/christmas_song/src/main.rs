// Training from : https://doc.rust-lang.org/stable/book/ch03-05-control-flow.html
// By Hasyame

use std::io::{self};

fn main() {
    let mut paragraphe = 0;
    let mut day_of = "";
    let mut partridge = "\nA partridge in a pear tree";
    let mut turtles = "";
    let mut french = "";
    let mut bird = "";
    let mut ring = "";
    let mut geese = "";
    let mut ladies = "";
    let mut maids = "";
    let mut swans = "";
    let mut lords = "";
    let mut pipers = "";
    let mut drummer = "";

    let tht = ["On the first day of Christmas,", "On the second day of Christmas,",
    "On the third day of Christmas,", "On the fourth day of Christmas,",
    "On the fifth day of Christmas,", "On the sixth day of Christmas,",
    "On the seventh day of Christmas,", "On the eighth day of Christmas,",
    "On the ninth day of Christmas,","On the tenth day of Christmas,",
    "On the eleventh day of Christmas,", "On the twelfth day of Christmas,"];

    // Début de la boucle
    for elem in tht.iter() {
        if paragraphe < 6 {
            day_of = " my true love sent to me";
        } else {
            day_of = " my true love gave to me";
        }

        if paragraphe >= 1 {
            partridge = "\nAnd partridge in a pear tree";
        }

        if paragraphe == 1 {
            turtles = "\nTwo turtle doves";
        } else if paragraphe > 1 {
            turtles = " two turtle doves";
        }

        if paragraphe == 2 {
            french = "\nThree French hens,";
        } else if paragraphe > 2 {
            french = " three French hens,";
        }

        if paragraphe >= 3 {
            bird = "\nFour calling birds,"
        }

        if paragraphe >= 4 {
            ring = "\nFive golden rings."
        }

        if paragraphe == 5 {
            geese = "\nSix geese a-laying,"
        } else if paragraphe > 5 {
            geese = " six geese a-laying,";
        }

        if paragraphe == 6 {
            swans = "\nSeven swans a-swimming,"
        } else if paragraphe == 11 {
            swans = "\nSeven swans a-swimming,"
        } else if paragraphe > 6 {
            swans = " seven swans a-swimming,"
        }

        if paragraphe == 7 {
            maids = "\nEight maids a-milking,"
        } else if paragraphe > 7 {
            maids = " eight maids a-milking,"
        }

        if paragraphe == 8 {
            ladies = "\nNine ladies dancing,"
        } else if paragraphe == 11 {
            ladies = "\nNine ladies dancing,"
        } else if paragraphe > 8 {
            ladies = " nine ladies dancing,"
        }

        if paragraphe == 9 {
            lords = "\nTen lords a-leaping,"
        } else if paragraphe > 9 {
            lords = " ten lords a-leaping,"
        }

        if paragraphe == 10 {
            pipers = "\nEleven pipers piping,"
        } else if paragraphe == 11 {
            pipers = "\nEleven pipers piping,"
        } else if paragraphe > 10 {
            pipers = " eleven pipers piping,"
        }

        if paragraphe == 11 {
            drummer = "\nTwelve drummers drumming,"
        }

        println!("{}{}{}{}{}{}{}{}{}{}{}{}{}{}\n", elem, day_of, drummer, pipers, lords, ladies,
        maids, swans, geese, ring, bird, french, turtles, partridge);
        paragraphe = paragraphe + 1
    }
    println!("Tapez une touche pour terminer le programme.");
    let mut the_end = String::new();
    io::stdin()
        .read_line(&mut the_end)
        .expect("Failed");
}
