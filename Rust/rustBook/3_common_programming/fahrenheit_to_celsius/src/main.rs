use std::io::{self, Write};

fn main() {
    let mut x = 1;
    while x == 1 {
        let mut y = 1;
        println!("Quel conversion souhaitez-vous faire ?");
        println!("Fahrenheit en Celsius tapez 1");
        println!("Celsius en Fahrenheit tapez 2");
        let mut choix_str = String::new();
        print!("Votre choix: ");
        let _ = io::stdout().flush();
        io::stdin()
            .read_line(&mut choix_str)
            .expect("\nFailed to read line");

        let choix_int: i32 = choix_str.trim().parse().unwrap();

        if choix_int == 1 {
            println!("\nVous avez choisis la convertion Fahrenheit to Celsius");
            far_cel();
            while y == 1 {
                println!("Voulez-vous recommencer? [O/N]");
                let mut continuer = String::new();
                io::stdin()
                    .read_line(&mut continuer)
                    .expect("Failed to read line");
                if continuer.trim() == "O" {
                    x = 1;
                    y = 0;
                } else if continuer.trim() == "N" {
                    x = 0;
                    y = 0;
                } else {
                    println!("Je n'ai pas compris votre réponse.");
                }
            }
        } else if choix_int == 2 {
            println!("\nVous avez choisis la convertion Celsius to Fahrenheit");
            cel_far();
            while y == 1 {
                println!("Voulez-vous recommencer? [O/N]");
                let mut continuer = String::new();
                io::stdin()
                    .read_line(&mut continuer)
                    .expect("Failed to read line");
                if continuer.trim() == "O" {
                    x = 1;
                    y = 0;
                } else if continuer.trim() == "N" {
                    x = 0;
                    y = 0;
                } else {
                    println!("Je n'ai pas compris votre réponse.");
                }
            }
        } else {
            println!("Pouvez-vous re-essayer ?");
        }
    }
    println!("Tapez une touche pour terminer le programme.");
    let mut the_end = String::new();
    io::stdin()
        .read_line(&mut the_end)
        .expect("Failed");
}

fn far_cel(){
    print!("Combiens de Fahrenheit en Celsius voulez-vous convertir: ");
    let _ = io::stdout().flush();
    let mut fahrenheit = String::new();
    io::stdin()
        .read_line(&mut fahrenheit)
        .expect("Failed to read line");

    let fahrenheit: f64 = fahrenheit.trim().parse().unwrap();
    let result = far_calc(fahrenheit);
    println!("\nRésultat: {}°F est égal à {}°C ", fahrenheit, result);
}

fn cel_far() {
    print!("Combiens de Celsius en Fahrenheit voulez-vous convertir: ");
    let _ = io::stdout().flush();
    let mut celsius = String::new();
    io::stdin()
        .read_line(&mut celsius)
        .expect("Failed to read line");

    let celsius: f64 = celsius.trim().parse().unwrap();
    let result = cel_calc(celsius);
    println!("\nRésultat: {}°C est égal à {}°F ", celsius, result);
}

fn cel_calc(celsius_numb: f64) -> f64 {
    (celsius_numb * 1.8) + 32.0
}

fn far_calc(fahrenheit_numb: f64) -> f64 {
    (fahrenheit_numb - 32.0) / 1.8000
}
